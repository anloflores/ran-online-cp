<?php

return [
    'title' => 'Mythical Glory Ran Online',
    'description' => 'Mythical Glory Ran Online Classic',
    
    'columns' => [
        'GameTime' => 'Gametime3'
    ],


    'voting' => [
        'is_level_req' => true,
        'level_req' => 210,
    ],

    'conversion' => [
        'ep_to_vp' => [
            'is_open'   => true,
            'min'       => 1,
            'to'        => 25,
        ],

        'gt_to_vp'  => [
            'is_open'   => true,
            'min'       => 60,
            'to'        => 1
        ],
    ]

];
