
@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel">
            <div class="panel-header">
                Top Up
            </div>
            <div class="panel-body">
               <form action="{{ route('topup.store') }}" method="POST">
                   @csrf

                   <div class="form-group">
                       <label for="">Code</label>
                       <input type="text" name="code" class="form-control">
                   </div>

                   <div class="form-group">
                       <label for="">PIN</label>
                       <input type="text" name="pin" class="form-control">
                   </div>

                   <button class="btn btn-gradient-primary float-right">Submit</button>
                   <div class="clearfix"></div>
               </form>
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')
        @include('includes.panels.top_5_rankings')
    </div>


<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection