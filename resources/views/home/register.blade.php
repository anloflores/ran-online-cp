
@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">


        <div class="panel">
            <div class="panel-header">
                Registration
            </div>
            <div class="panel-body news-container">
                @include('includes.msg.errors')
                @include('includes.msg.success')
                <form action="{{ route('register.process') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="Username">
                    </div>

                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>

                    <div class="form-group">
                        <label for="">Re-Password</label>
                        <input type="password" name="confirm_password" class="form-control" placeholder="Re-Password">
                    </div>

                    <div class="form-group">
                        <label for="">Pincode</label>
                        <input type="password" name="pincode" class="form-control" placeholder="Pincode">
                    </div>

                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
                    </div>

                    <div class="float-left">
                        <div class="g-recaptcha" data-sitekey="6Le8NfsUAAAAAHiKYyIeVvnsIlHEnoOmGFv5IJbS"></div>
                    </div>

                    <div class="float-right">
                        <button type="submit" class="btn btn-gradient-primary">Sign Up</button>
                    </div>

                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>


<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection