
@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel">
            <div class="panel-header">
                Vote
            </div>
            <div class="panel-body item-shop-container">
                <div style="width: 100%;">
                    <p style="margin-bottom: 0px;">Vote for us to earn Vote Points.</p>
                    <p style="margin-top: 0px;">You can use Vote Points to buy an Item on Item Shop.</p>
                    <hr>
                </div>
                
                @foreach ($vote_links as $v)
                    @php
                        $votable = $get_last($v->id);
                    @endphp
                    <div class="item-shop-item">
                            <div class="item-shop-item-img">
                                <img src="{{ asset('votes/'. $v->img) }}" alt="">
                            </div>

                            <div class="item-shop-item-details">
                                <p>{{ $v->name  }}</p>
                                <p class="text-gold">{{  number_format($v->reward) }} vPoints</p>
                                @auth
                                    <form data-alert="buy" action="{{ route('vote.store') }}" method="POST" class="float-left">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $v->id }}">
                                        <button class="btn btn-primary btn-small" {{ !$votable ? 'disabled' : '' }}>{{ $votable ? 'Vote' : 'Already Voted' }}</button>
                                    </form>
                                @endauth
                            </div>
                        </div>
                @endforeach
            </div>
        </div>

        <div class="panel">
            <div class="panel-header">
                History
            </div>
            <div class="panel-body">
                <ul>
                    @foreach ($last as $item)
                        <li>Voted at <span class="text-gold">{{ $item->name }}</span> with an IP of <span class="text-gold">{{ $item->ip }}</span> on <span class="text-gold">{{ date('Y-m-d H:i:s', strtotime($item->created_at)) }}</span></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')
        @include('includes.panels.top_5_rankings')
    </div>


<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection