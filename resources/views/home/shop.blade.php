@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">

        <div class="panel">
            <div class="panel-header">
                Search Item
            </div>
            <div class="panel-body">
                <form action="" method="GET">
                    <div class="form-group">
                        <label for="">Search Item</label>
                        <input type="text" name="name" value="{{ request()->name }}" class="form-control">
                    </div>
                </form>
            </div>
        </div>

        <div class="panel">
            <div class="panel-header">
                Item Shop
            </div>
            <div class="panel-body  item-shop-container">
                @if (count($items) <= 0)
                 <p class="text-center">No Items Available</p>
                @else
                    @foreach ($items as $i)
                        <div class="item-shop-item">
                            <div class="item-shop-item-img">
                                <img src="{{ 
                                    is_file(public_path('items/' . $i->ItemSS)) ? asset('items/' . $i->ItemSS) : asset('items/box.png')
                                }}" alt="">
                            </div>

                            <div class="item-shop-item-details">
                                <p>{{ $i->ItemName  }}</p>
                                <p class="text-gold">{{  number_format($i->ItemPrice) }} {{ $i->ItemSec == 1 ? 'ePoints' : 'vPoints'}}</p>
                                @auth
                                    <form data-alert="buy" action="{{ route('shop.buy.process') }}" method="POST" class="float-left">
                                        @csrf
                                        <input type="hidden" name="ItemName" value="{{ $i->ItemName }}">
                                        <input type="hidden" name="ProductNum" value="{{ $i->ProductNum }}">
                                        <button class="btn btn-primary btn-small">BUY</button>
                                    </form>
                                @endauth
                                <button class="btn btn-primary btn-small view-item" data-record="{{ base64_encode(json_encode($i)) }}" data-toggle="modal" data-target="viewItem">VIEW</button>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="right-container">
        <div class="panel">
            <div class="panel-header">
                Options
            </div>
            <div class="panel-body">
                <ul>
                    @foreach (\App\Http\Controllers\Helper::$_shopCategories as $k => $item)
                        <li><a href="{{ request()->fullUrlWithQuery(['category' => $k]) }}">{{ $item }}</a></li>
                    @endforeach
                </ul>
                <hr>
                <ul>
                    <li><a href="{{ request()->fullUrlWithQuery(['type' => '1']) }}">ePoints</a></li>
                    <li><a href="{{ request()->fullUrlWithQuery(['type' => '2']) }}">vPoints</a></li>
                </ul>
            </div>
        </div>
        @include('includes.panels.status')
        @include('includes.panels.top_5_rankings')
    </div>



      <div class="modal" id="viewItem">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="font-kanit m-0 float-left" data-column="ItemName">Test</h2>
                <div class="float-right"><i class="fas fa-times" data-modal="close"></i></div>
                <div class="clearfix"></div>
                <hr>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img data-column="ItemSS" src="{{ asset('items/box.png') }}" alt="">
                    <p>Price : <span class="text-gold" data-column="ItemPrice"></span> <span class="text-gold" data-column="ItemSec"></span></p>
                    <p data-column="ItemComment"></p>
                    <div class="text-center">
                        <form data-alert="buy" action="{{ route('shop.buy.process') }}" method="POST">
                            @csrf
                            <input type="hidden" data-column="ItemName" data-type="input" name="ItemName" value="">
                            <input type="hidden" data-column="ProductNum" name="ProductNum" value="">
                            <button class="btn btn-gradient-primary">Buy</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script>
        $('.view-item').click(function() {
            var d = JSON.parse(atob($(this).data('record')));
            $('#viewItem').find('[data-column]').each((i, e) => {
                var col = $(e).data('column');

                if(col == 'ItemSS') {
                    $(e).attr('src', (d[col] == '' ? '{{ asset("items/box.png") }}' : '{{ asset("items") }}/' + d[col]))
                } else if(col == 'ItemSec') {
                    $(e).text(d[col] == 1 ? 'ePoints' : 'vPoints');
                } else if(col == 'ProductNum') {
                    $(e).val(d[col]);
                } else {
                    var type = $(e).data('type');
                    if(type == 'input') {
                        $(e).val(d[col]);
                    } else {
                        $(e).text(d[col]);
                    }
                }
            })
        })

        $('[data-alert="buy"]').on('submit', function() {
            var name = $(this).find('[name="ItemName"]').val();
            if(confirm('Are you sure you want to purchase '+ name +'?')) {
                return true;
            } else {
                return false;
            }
        }); 
    </script>
@endsection