@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">


        <div class="panel">
            <div class="panel-header">
                {{ $title }}
            </div>
            <div class="panel-body news-container text-center">
                {{ $content }}
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>
@endsection