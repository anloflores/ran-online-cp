@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">


        <div class="panel">
            <div class="panel-header">
                {{ $news->title ?? 'Not Found' }}
            </div>
            <div class="panel-body news-container">
                @if ($news)
                    <span class="small">Date Created : <span class="color-gold">{{ $news->created_at }}</span> | Updated at : <span class="color-gold">{{ $news->updated_at }}</span></span>
                    <hr>
                    {!! $news->content !!}
                @else
                    <div class="text-center">
                        Content is unavailable.
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>
@endsection