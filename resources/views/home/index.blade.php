@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel slider-container">
            <div class="slider">
                @php
                    $images = DB::connection('RanComplexusWeb')
                        ->table('dbo.Images')
                        ->get();
                @endphp
                <ul class="slider-items">
                    @foreach ($images as $k => $item)
                        <li class="{{ $k == 0 ? 'active' : '' }}" style="background-image: url({{ asset('slider/'.$item->name) }});"></li>
                    @endforeach
                </ul>

                <ul class="slider-control">
                    <li class="prev" style="cursor: pointer;">
                        <i class="fa fa-chevron-left"></i>
                    </li>
                    <li class="next" style="cursor: pointer;">
                        <i class="fa fa-chevron-right"></i>
                    </li>
                </ul>
            </div>
        </div>


        <div class="panel">
            <div class="panel-header">
                News & Update
            </div>
            <div class="panel-body news-container">
                @foreach($news as $n)
                    <div class="news-item">
                       <a href="{{ route('news', $n->id) }}" style="color: inherit;">
                         @switch($n->type)
                            @case(1)
                                <span class="text-red text-uppercase">News</span>
                            @break
                            @case(2)
                                <span class="text-gold text-uppercase">Update</span>
                            @break
                            @case(3)
                                <span class="text-red text-uppercase">Hot</span>
                            @break
                        @endswitch
                        <span>{{ $n->title }}</span>
                        <span class="text-gray text-uppercase">{{ date('m.d.Y', strtotime($n->created_at)) }}</span>
                       </a>
                    </div>
                @endforeach
            </div>
        </div>


        @if(count($items) >= 2)
            <div class="panel">
                <div class="panel-header">
                    Item Shop
                </div>
                <div class="panel-body item-shop-container">

                    @foreach ($items as $i)
                        <div class="item-shop-item">
                            <div class="item-shop-item-img">
                                <img src="{{ 
                                    is_file(asset('items/' . $i->ItemSS)) ? asset('items/' . $i->ItemSS) : asset('items/box.png')
                                }}" alt="">
                            </div>

                            <div class="item-shop-item-details">
                                <p>{{ $i->ItemName  }}</p>
                                <p class="text-gold">{{  number_format($i->ItemPrice) }} {{ $i->ItemSec == 1 ? 'ePoints' : 'vPoints'}}</p>
                                @auth
                                    <button class="btn btn-primary btn-small">BUY</button>
                                @endauth
                                <button class="btn btn-primary btn-small" data-record="{{ base64_encode(json_encode($i)) }}" data-toggle="modal" data-target="viewItem">VIEW</button>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        @endif
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>






@endsection

@section('script')

    <div class="modal" id="viewItem">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="font-kanit m-0 float-left" data-column="ItemName">Test</h2>
                <div class="float-right"><i class="fas fa-times" data-modal="close"></i></div>
                <div class="clearfix"></div>
                <hr>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img data-column="ItemSS" src="{{ asset('items/box.png') }}" alt="">
                    <p>Price : <span class="text-gold" data-column="ItemPrice">9,999</span> <span class="text-gold" data-column="ItemSec">ePoints</span></p>
                    <p data-column="ItemComment">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iure sed nam accusamus quia dolorem ipsa quis consequuntur quas fuga numquam officia, suscipit quae aspernatur praesentium hic voluptatum neque doloribus laudantium.</p>
                    <button class="btn btn-gradient-primary">Buy</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('[data-toggle="modal"]').click(function() {
            var d = JSON.parse(atob($(this).data('record')));
            $('#viewItem').find('[data-column]').each((i, e) => {
                var col = $(e).data('column');

                if(col == 'ItemSS') {
                    $(e).attr('src', (d[col] == '' ? '{{ asset("items/box.png") }}' : '{{ asset("items") }}/' + d[col]))
                } else if(col == 'ItemSec') {
                    $(e).text(d[col] == 1 ? 'ePoints' : 'vPoints');
                } else {
                    $(e).text(d[col]);
                }
            })
        })
    </script>
@endsection