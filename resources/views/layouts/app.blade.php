<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="{{ asset('assets/img/banner.jpg') }}" />
    <meta property="og:description" content="{{ $_settings('description') }}" />
    <meta property="og:url"content="{{ URL::to('/') }}" />
    <meta property="og:title" content="{{ $_settings('title') }}" />

    <title>{{ $_settings('title') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</head>
<body>
    <div class="header">
        <nav class="menu">
            <ul>
                <li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}">News</a></li>
                <li class="{{ Route::currentRouteName() == 'rankings' ? 'active' : '' }}"><a href="{{ route('rankings') }}">Rankings</a></li>
                <li class="{{ Route::currentRouteName() == 'downloads' ? 'active' : '' }}"><a href="{{ route('downloads') }}">Downloads</a></li>
                @auth
                    <li class="{{ Route::currentRouteName() == 'shop' ? 'active' : '' }}"><a href="{{ route('shop') }}">Item Shop</a></li>
                @endauth
                {{-- <li>Item Shop</li>
                <li>Faqs</li> --}}
            </ul>
        </nav>
    </div>
    <div class="container main-content">
        @yield('content')
    </div>
</body>

<footer>
    <div class="container">
        <div class="left-container"></div>
        <div class="main-container"></div>
        <div class="rigth-container text-center">
            <div class="text-gold">Crafted by</div>
            <img src="http://complexus.net/images/logo.png" width="150" alt=""><br/>
            <label for="">C O M P L E X U S</label>
        </div>
    </div>
</footer>

<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
   $(function() {
        $('[data-toggle="modal"]').click(function() {
            var target = $(this).attr('data-target');
            $('#' + target).fadeIn("fast");
        });

        $('[data-modal="close"]').click(function() {
            $('.modal').fadeOut();
        });
   })
</script>
@if (session('success') !== null)
    @if(session('success') == true)
        <script>
            toastr.success("{{ session('msg') ?? 'Action Success' }}")
        </script>
    @endif

    @if(session('success') == false)
        <script>
            toastr.error("{{ session('msg') ?? 'Action Success' }}")
        </script>
    @endif
@endif
<script>
    
    //Slider
        var speed = 5000; // milisecond
        var current = $('.slider-items > li.active').index();
        var max = $('.slider-items > li').length-1;

        var slider = () => {
            if(current == max) {
                current = 0;
            } else {
                current++;
            }

            $('.slider-items > li').removeClass('active');
            $('.slider-items > li').eq(current).addClass('active');
        }

        var interval = setInterval(function() {
            slider()
        }, speed)

        // Slider Control

        $('.slider-control > .prev').click(function() {

            clearInterval(interval);

            if(current == 0) {
                current = max;
            } else {
                current--;
            }

            $('.slider-items > li').removeClass('active');
            $('.slider-items > li').eq(current).addClass('active');

            interval = setInterval(function() {
                slider()
            }, speed)
        });

        $('.slider-control > .next').click(function() {

            clearInterval(interval);

            if(current == max) {
                current = 0;
            } else {
                current++;
            }

            $('.slider-items > li').removeClass('active');
            $('.slider-items > li').eq(current).addClass('active');

            interval = setInterval(function() {
                slider()
            }, speed)
        });


        

</script>

@yield('script')
</html>