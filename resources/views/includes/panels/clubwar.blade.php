@php
    $clubwar = Illuminate\Support\Facades\Cache::remember('clubwar_winner', 60, function() {
        return DB::connection('RanGame1')
        ->table('dbo.GuildRegion')
        ->selectRaw("
            (CASE
                WHEN RanGame1.dbo.GuildRegion.RegionID = 0 THEN 'SG'
                WHEN RanGame1.dbo.GuildRegion.RegionID = 1 THEN 'MP'
                WHEN RanGame1.dbo.GuildRegion.RegionID = 2 THEN 'PHX'
                WHEN RanGame1.dbo.GuildRegion.RegionID = 3 THEN 'TH'
                WHEN RanGame1.dbo.GuildRegion.RegionID = 4 THEN 'GW'
            END) as Region,
            RanGame1.dbo.GuildRegion.RegionTax as RegionTax,
            RanGame1.dbo.GuildInfo.GuNum as GuNum,
            RanGame1.dbo.GuildInfo.GuName as GuName,
            RanGame1.dbo.GuildInfo.GuRank as GuRank,
            RanGame1.dbo.ChaInfo.ChaName as ChaName
        ")
        ->leftJoin('RanGame1.dbo.GuildInfo', 'RanGame1.dbo.GuildInfo.GuNum', '=', 'RanGame1.dbo.GuildRegion.GuNum')
        ->leftJoin('RanGame1.dbo.ChaInfo', 'RanGame1.dbo.ChaInfo.ChaNum', '=', 'RanGame1.dbo.GuildInfo.ChaNum')
        ->first();
    });
@endphp

@if($clubwar)
<div class="panel">
    <div class="panel-header">
        Club War
    </div>
    <div class="panel-body school-war-container">
        <br/>
        <div class="school-war-item">
            <div class="float-left school-war-item-img">
                <img src="/assets/img/phx.png" alt="">
            </div>
            <div class="float-left school-war-item-details">
                <h3 class="font-kanti">{{ $clubwar->GuName }}</h3>
                <p class="school-war-item-details-school">
                    @php
                        switch($clubwar->Region) {
                            case 'SG': echo "Sacred Gate"; break;
                            case 'MP': echo "Mystic Peak"; break;
                            case 'PHX': echo "Phoenix"; break;
                            case 'TH': echo "Trading Hole"; break;
                            case 'GW': echo "GW"; break;
                        }
                    @endphp
                </p>
                <p>Level : <span class="text-gold">{{ $clubwar->GuRank }}</span></p>
                <p><span class="text-gold">{{ $clubwar->ChaName }}</span></p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@endif