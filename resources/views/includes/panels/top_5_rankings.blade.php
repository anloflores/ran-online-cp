@php
    $five = Cache::remember('playerTopFive', 60*12, function() {
        return DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->selectRaw("
                RanGame1.dbo.ChaInfo.ChaName as ChaName, 
                RanGame1.dbo.GuildInfo.GuName as ChaGuName, 
                (CASE
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(1,64) THEN 'Brawler'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(2,128) THEN 'Swordsman'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(4,256) THEN 'Archer'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(8,512) THEN 'Shaman'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(1024,2048) THEN 'Gunner'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(4096,8192) THEN 'Assassin' 
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(16384,32768) THEN 'Magician'
                END) as ChaClass, 
                RanGame1.dbo.ChaInfo.ChaClass as ChaClassNum,
                (CASE 
                    WHEN RanGame1.dbo.ChaInfo.ChaSchool = 0 THEN 'SG'
                    WHEN RanGame1.dbo.ChaInfo.ChaSchool = 1 THEN 'MP'
                    WHEN RanGame1.dbo.ChaInfo.ChaSchool = 2 THEN 'PHX'
                    WHEN ChaSchool NOT IN(0, 1, 2) THEN 'Leonair'
                END) as ChaSchool, 
                RanGame1.dbo.ChaInfo.ChaLevel as ChaLevel, 
                RanGame1.dbo.ChaInfo.ChaMoney as ChaMoney, 
                RanGame1.dbo.ChaInfo.ChaOnline as ChaOnline, 
                RanGame1.dbo.ChaInfo.ChaReborn as ChaReborn, 
                RanGame1.dbo.ChaInfo.ChaPkLoss as ChaPkLoss, 
                RanGame1.dbo.ChaInfo.ChaPkWin as ChaPkWin, 
                RanGame1.dbo.ChaInfo.ChaKills as ChaKills")
            ->leftJoin('RanUser.dbo.UserInfo', 'RanUser.dbo.UserInfo.UserNum', '=', 'RanGame1.dbo.ChaInfo.UserNum')
            ->leftJoin('RanGame1.dbo.GuildInfo', 'RanGame1.dbo.GuildInfo.GuNum', '=', 'RanGame1.dbo.ChaInfo.GuNum')
            ->where('RanUser.dbo.UserInfo.UserType', '1')
            ->orderBy('RanGame1.dbo.ChaInfo.ChaLevel', 'DESC')
            ->orderBy('RanGame1.dbo.ChaInfo.ChaPkWin', 'DESC')
            ->orderBy('RanGame1.dbo.ChaInfo.ChaPkLoss', 'ASC')
            ->limit(5)
            ->get();
    });
@endphp


<div class="panel">
    <div class="panel-header">
        Rankings
    </div>
    <div class="panel-body rankings-container">

        @foreach ($five as $k => $c)
            <div class="rankings-item">
                <div class="rankings-item-img">
                    <img src="{{ asset('assets/img/class/'.$c->ChaClassNum.'.jpg') }}" alt="" style="border-radius: 8px; {{ $c->ChaOnline == 1 ? 'border: 1px solid green;' : 'border: 1px solid red;' }}">
                </div>

                <div class="rankings-item-details">
                    <h3 class="font-kanit"><span class="text-gray">{{ $k+1 }}.</span> {{ substr($c->ChaName, 0, 13) }}{{ strlen($c->ChaName) >= 13 ? '...' : '' }} </h3>
                    <p class="text-gray">Lvl : <span class="text-gold">{{ $c->ChaLevel }}</span> - <span class="text-gold">{{ $c->ChaSchool }}</span></p>
                    <p class="text-gray">PK W/L : <span class="text-gold">{{ $c->ChaPkWin }}/{{ $c->ChaPkLoss }}</span></p>
                    <p class="text-white">{{ $c->ChaGuName }}</p>
                </div>
            </div>
        @endforeach

    </div>
</div>