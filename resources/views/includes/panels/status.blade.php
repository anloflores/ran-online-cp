  @php
    $online = DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->selectRaw('COUNT(*) as total')
            ->where('ChaOnline', 1)
            ->first();

    $characters = Cache::remember('totalCharacters', 60, function() {
        return DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->selectRaw('COUNT(*) as total')
            ->first();
    });

    $gangs = Cache::remember('totalGangs', 60, function() {
        return DB::connection('RanGame1')
            ->table('dbo.GuildInfo')
            ->selectRaw('COUNT(*) as total')
            ->first();
    });
@endphp
<div class="panel">
    <div class="panel-header">
        Status
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="">Server Status</label>
            <h2 class="font-kanit text-green m-0 text-uppercase">Online</h2>
        </div>
        <div class="form-group">
            <label for="">Statistics</label>
            <h4 class="font-kanit m-0 text-uppercase"><span class="text-green">{{ number_format(floor($online->total * $_settings('online_multiplier') )) }}</span> Online Players right now</h4>
            <h4 class="font-kanit m-0 text-uppercase"><span class="text-green">{{ number_format($gangs->total) }}</span> Total Gangs</h4>
        </div>
    </div>
</div>