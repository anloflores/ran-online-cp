@auth
    @php
        $characters = Cache::remember('charactersNo', 60, function() {
            return DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->selectRaw('COUNT(UserNum) as total')
            ->where('UserNum', Auth::user()->UserNum)
            ->first();
        });

        $points = DB::table('dbo.Points')
        ->where('UserNum', Auth::user()->UserNum)
        ->first();

    @endphp

    <style>
        .user-option-action {
            margin-bottom: 10px;
            display: inline-block;
        }
    </style>


    <div class="panel">
        <div class="panel-header">
            {{ Auth::user()->UserName }}
        </div>
        <div class="panel-body">
            <div class="text-gray" style="padding: 15px">
                ePoints : <span class="text-gold">{{ $points->ePoints  }}</span>
                <br/>
                vPoints : <span class="text-gold">{{ $points->vPoints  }}</span>
            </div>
            <div class="clearfix"></div>
            <hr>
            <br/>
            <a href="{{ route('characters') }}" class="label label-primary user-option-action">
                Characters ({{ $characters->total }})
            </a><br/>
            <hr />
            <a href="{{ route('change_password') }}" class="label label-primary user-option-action">Change Password</a><br/>
            <a href="{{ route('change_email') }}" class="label label-primary user-option-action">Change Email</a>
            <hr />
            @if (App\Http\Controllers\Helper::$_activeUserFunctions['change_school'])
                <a href="{{ route('change_school') }}" class="label label-primary user-option-action">Change School</a>
            @endif

            @if (App\Http\Controllers\Helper::$_activeUserFunctions['gt2vp'])
                <a href="{{ route('topup') }}" class="label label-primary user-option-action">Top Up</a>
                <a href="{{ route('gt_to_vp') }}" class="label label-primary user-option-action">Game Time to vPoints</a>
                <a href="{{ route('vote') }}" class="label label-primary user-option-action">Earn vPoints</a>
            @endif
            <br/><br/>
            <div class="text-center">
                <a href="{{ route('auth.logout') }}" class="label label-danger user-option-action">Logout</a>
            </div>
        </div>
    </div>
@else
    <div class="panel">
        <div class="panel-header">
            Login
        </div>
        <div class="panel-body">
            @if (session('error'))
                <div class="alert alert-danger small">{{ session('msg') }}</div>
            @endif
            <form action="{{ route('auth') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" name="username" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group float-left" style="margin-top: 10px;">
                    <a href="#">Forgot Password?</a>
                </div>

                <div class="form-group float-right">
                    <button class="btn btn-gradient-primary">Login</button>
                </div>
                <div class="clearfix"></div>
            </form>

            <hr>

            <div class="text-center form-group">
                <p>Not registered yet?</p>
                <a href="{{ route('register') }}">
                    <button class="btn btn-gradient-primary">Register Now!</button>
                </a>
            </div>
        </div>
    </div>   
@endauth