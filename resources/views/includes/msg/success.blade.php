@if (session('success') !== null && session('success') === true)
    <div class="alert alert-success">
        {{ session('msg') }}
    </div>
@endif

@if (session('success') !== null && session('success') === false)
    <div class="alert alert-danger">
        {{ session('msg') }}
    </div>
@endif