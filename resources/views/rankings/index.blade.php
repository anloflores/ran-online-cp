@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel">
            <div class="panel-header">
                Rankings
            </div>
            <div class="panel-body rankings-container">
                <p class="m-0 text-gray">The ranking result is not realtime. Date Cache : <span class="text-gold">{{ date('m/d/Y h:m:i', $top['date']) }}</span></p>
                <hr>
                <Br/>
                @foreach ($top['data'] as $k => $c)
                    @php
                        $page = 1;
                        if(Request::input('page') !== null && Request::input('page') != 1) {
                            $k++;
                            $page = Request::input('page')*10-10;
                        }
                    @endphp
                    
                    <div class="rankings-item">
                        <div class="rankings-item-img">
                            <img src="{{ asset('assets/img/class/'. $c->ChaClassNum . '.jpg') }}" alt="" style="border-radius: 8px; {{ $c->ChaOnline == 1 ? 'border: 1px solid green;' : 'border: 1px solid red;' }}">
                        </div>

                        <div class="rankings-item-details">
                            <h3 class="font-kanit"><span class="text-gold">{{ $k+$page }}.</span> {{ $c->ChaName }} </h3>
                            <p class="text-gray">
                                Lvl : <span class="text-gold">{{ $c->ChaLevel }}</span> - <span class="text-gold">{{ $c->ChaSchool }}</span>
                                 @if($c->ChaGuName != '')  - Gang : <span class="text-gold">{{ $c->ChaGuName }}</span> @endif
                                 @if(isset($c->LevelUpDate) && $c->ChaLevel == $maxLevel)  - Date Capped : <span class="text-gold">{{ date('m/d/Y H:i:s', strtotime($c->LevelUpDate)) }}</span> @endif
                            </p>
                            <p class="text-gray">Class: <span class="text-gold">{{ $c->ChaClass }}</span> • LVL : <span class="text-gold">{{ $c->ChaLevel }}</span> • PK W/L : <span class="text-gold">{{ $c->ChaPkWin }}</span>/<span class="text-gold">{{ $c->ChaPkLoss }}</span> • Wealth : <span class="text-gold">{{ number_format($c->ChaMoney) }}</span></p>
                        </div>
                    </div>
                @endforeach

                <div class="text-center">
                    {{ $top['data']->appends($_GET)->links() }}
                </div>
                
            </div>
        </div>
    </div>


    <style>
        .rank-options {
            margin-bottom: 10px;
            display: inline-block;
        }
    </style>

    <div class="right-container">

        <div class="panel">
            <div class="panel-header">
                Options
            </div>
            <div class="panel-body">

                <a class="rank-options" href="{{ route('rankings') }}"><span class="label label-primary">Reset</span></a>
                <br/>
                <br/>   
                <h5 class="font__qs clr__label" style="margin-top: 0px;">CLASSES</h5>
                @foreach (App\Http\Controllers\Helper::$_activeClasses as $k => $c)
                    @if(!$c) @continue @endif
                    <a class="rank-options" href="?class={{ $k }}"><span class="label label-primary">{{ ucwords($k) }}</span></a>
                @endforeach
                <br>
                <h5 class="font__qs clr__label">RANK BY</h5>
                <a class="rank-options" href="?sortBy=level"><span class="label label-danger">Level</span></a>
                <a class="rank-options" href="?sortBy=wealth"><span class="label label-danger">Wealth</span></a>
                <a class="rank-options" href="?sortBy=pk"><span class="label label-danger">PK</span></a>

                <br>
                <h5 class="font__qs clr__label">SCHOOL</h5>
                <a class="rank-options" href="?school=SG"><span class="label label-success">Sacred Gate</span></a>
                <a class="rank-options" href="?school=PHX"><span class="label label-success">Phoenix</span></a><br/>
                <a class="rank-options" href="?school=MP"><span class="label label-success">Mystic Peak</span></a>

            </div>
        </div>
        @include('includes.panels.status')
    </div>
@endsection