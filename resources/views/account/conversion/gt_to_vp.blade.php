@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel">
            <div class="panel-header">
                Game Time to Vote Points
            </div>
            <div class="panel-body">
                <h4 class="title font__pd clr__white">Mechanics:</h4>
                <p class="small">{{ $_settings('gt2vp')['conversion'][0] }} minutes = {{ $_settings('gt2vp')['conversion'][1] }} vPoint

                <hr />
                    <div class="alert alert-info small">Current Game Time : <span class="text-warning">{{ Auth::user()[$_settings('columns')['GameTime']] }}</span></div>
                <hr>

                @include('includes.msg.errors')
                <form action="{{ route('gt_to_vp.process') }}" method="POST">
                    {{ csrf_field() }}

                    @php
                        $con = $_settings('gt2vp')['conversion'][0];
                        $multiplier = $_settings('gt2vp')['conversion'][1];
                        $current = (int) Auth::user()[$_settings('columns')['GameTime']];
                        $amount = floor($current / $con) * $multiplier;
                        $converted = (int) $amount * (int) $con;
                        $balance = $current - $amount;
                    @endphp 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">vPoints <small>(to receive)</small></label>
                                <input type="text" class="form-control" readonly name="vp" value="{{ $amount }}">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-gradient-primary">Convert</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>
@endsection

@section('script')

            <script>
        $(function() {
            $('input[name="gt"]').keyup(function() {
                var v = parseInt($(this).val());
                var r = parseInt(v/{{ App\Model\Settings::$gt_to_vp['conversion'][0] }});
                $('input[name=vp]').val(r);
            });
        });
    </script>
@endsection