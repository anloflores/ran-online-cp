@extends('layouts.app')

@section('content')
<style>
    .table>tbody>tr>td {
        border-top: 1px solid rgba(255, 255, 255, 0.1);
    }
</style>
<h4 class="title font__pd clr__white">Conversion (ePoints to vPoints)</h4>
<br>
    <div class="__panel __lower_margin">
        <div class="__panel_content">
            @include('includes.msg.errors')
            @include('includes.msg.success')
            <form action="{{ route('change_password.process') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="__form-group">
                            <label for="">Current Email</label>
                            <input type="password" name="old_email" placeholder="Current Email">
                        </div>
                        <div class="__form-group">
                            <label for="">New Email</label>
                            <input type="password" name="new_email" placeholder="New Email">
                        </div>
                        <div class="__form-group">
                            <label for="">Confirm New Email</label>
                            <input type="password" name="confirm_new_email" placeholder="Confirm New Email">
                        </div>

                        <div class="pull-right">
                            <button type="submit" class="__main_btn">Change Email</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection