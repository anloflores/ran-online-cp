@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel">
            <div class="panel-header">
               Change Password
            </div>
            <div class="panel-body">
                @include('includes.msg.errors')
                @include('includes.msg.success')
                 <form action="{{ route('change_password.process') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Current Password</label>
                                <input type="password" name="old_password" class="form-control" placeholder="Current Password">
                            </div>
                            <div class="form-group">
                                <label for="">New Password</label>
                                <input type="password" name="new_password" class="form-control" placeholder="New Password">
                            </div>
                            <div class="form-group">
                                <label for="">Confirm New Password</label>
                                <input type="password" name="confirm_new_password" class="form-control" placeholder="Confirm New Password">
                            </div>

                            <div class="pull-right">
                                <button type="submit" class="btn btn-primary">Change Password</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>
@endsection