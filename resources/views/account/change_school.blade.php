@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel">
            <div class="panel-header">
               Change School
            </div>
            <div class="panel-body">
                <h4 class="title font__pd clr__white text-gray m-0">Amount:</h4>
                <p class="small m-0">
                    {{ number_format(App\Model\Settings::$change_school['amount']) }} worth of {{ App\Model\Settings::$change_school['currency'] }}
                </p>
                <hr>
                @include('includes.msg.errors')
                @include('includes.msg.success')
                <form action="{{ route('change_school.process') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Character</label>
                                <select name="character" id="" class="form-control">
                                    <option value="">-- Select Character --</option>
                                    @foreach ($characters as $item)
                                        <option value="{{ $item->ChaNum }}">{{ $item->ChaName }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">School</label>
                                <select name="school" id="" class="form-control">
                                    <option value="">-- Select School --</option>
                                    <option value="0">Sacred Gate</option>
                                    <option value="1">Mystic Peak</option>
                                    <option value="2">Phoenix</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-primary">Change School</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>
@endsection