@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

<style>
    .table {
        width:  100%;
    }
    .table>tbody>tr>td {
        border-top: 1px solid rgba(255, 255, 255, 0.1);
    }
</style>

<div class="main-container">
    @foreach ($characters as $c)
    <div class="panel __lower_margin">
        <div class="panel-header">
            <div class="clr__white float-left">
                <img style="height: 30px; width: 30px; border-radius: 50%; margin-right: 20px;" class="float-left" src="{{ asset('assets/img/class/' . $c->ChaClass . '.jpg') }}" alt="">
                {{ $c->ChaName }}
                @if ($c->GuNum)
                    <div class="clr__white"><img src="{{ route('gu_badge', $c->GuNum) }}" alt="">{{ $c->ChaGuName }}</div>
                @endif
            </div>

            <div class="float-right">
                <div class="label label-{{ $c->ChaOnline == 1 ? 'success' : 'danger' }} small pull-right" style="height: 12px;
                width: 12px;
                border-radius: 50% !important;
                padding: 0px;
                box-shadow: 0px 0px 10px rgba(0, 0,0, 0.1);" title="{{ $c->ChaOnline == 1 ? 'Online' : 'Offline'}}">&nbsp;</div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div>
                <br>
                <p class="clr__white">
                    <h4 class="title font__pd clr__white">Details</h4>
                    <table class="table small">
                        <tr>
                            <td class="text-warning">HP</td>
                            <td class="clr__white">{{ number_format($c->ChaHP) }}</td>
                            <td class="text-warning">MP</td>
                            <td class="clr__white">{{ number_format($c->ChaMP) }}</td>
                        </tr>
                        <tr>
                            <td class="text-warning">Level</td>
                            <td class="clr__white">{{ $c->ChaLevel }}</td>
                            <td class="text-warning">Class</td>
                            <td class="clr__white">{{ App\Http\Controllers\Helper::_getClass($c->ChaClass) }}</td>
                        </tr>
                        <tr>
                            @php
                                $school = App\Http\Controllers\Helper::_getSchool($c->ChaSchool);
                            @endphp
                            <td class="text-warning">School</td>
                            <td class="clr__white">{{ $school }} <img src="{{ asset('images/school/mini/' . $school . '.png') }}" alt=""></td>
                            <td class="text-warning">Gold</td>
                            <td class="clr__white">{{ number_format($c->ChaMoney) }}</td>
                        </tr>
                    </table>
                    <h4 class="title font__pd clr__white">PK</h4>
                    <table class="table small">
                        <tr>
                            <td class="text-warning">Win</td>
                            <td class="clr__white">{{ number_format($c->ChaPkWin) }}</td>
                            <td class="text-warning">Loss</td>
                            <td class="clr__white">{{ number_format($c->ChaPkLoss) }}</td>
                        </tr>
                    </table>
                    <h4 class="title font__pd clr__white">Stats</h4>
                    <table class="table small">
                        <tr>
                            <td class="text-warning">Pow</td>
                            <td class="clr__white">{{ number_format($c->ChaPower) }}</td>
                            <td class="text-warning">Dex</td>
                            <td class="clr__white">{{ number_format($c->ChaDex) }}</td>
                        </tr>
                        <tr>
                            <td class="text-warning">Int</td>
                            <td class="clr__white">{{ number_format($c->ChaSpirit) }}</td>
                            <td class="text-warning">Stm</td>
                            <td class="clr__white">{{ number_format($c->ChaStrength) }}</td>
                        </tr>
                        <tr>
                            <td class="text-warning">Vit</td>
                            <td class="clr__white">{{ number_format($c->ChaStrong) }}</td>
                            <td class="text-warning">Free</td>
                            <td class="clr__white">{{ number_format($c->ChaStRemain) }}</td>
                        </tr>
                    </table>
                </p>
            </div>
        </div>
    </div>
@endforeach
</div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>
@endsection