@extends('layouts.app')

@section('content')
    <div class="left-container">
        @include('includes.panels.login')
        @include('includes.panels.clubwar')
    </div>

    <div class="main-container">
        <div class="panel">
            <div class="panel-header">
               Change Email
            </div>
            <div class="panel-body">
                @include('includes.msg.errors')
                @include('includes.msg.success')
                <form action="{{ route('change_password.process') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Current Email</label>
                                <input type="password" name="old_email" class="form-control" placeholder="Current Email">
                            </div>
                            <div class="form-group">
                                <label for="">New Email</label>
                                <input type="password" name="new_email" class="form-control" placeholder="New Email">
                            </div>
                            <div class="form-group">
                                <label for="">Confirm New Email</label>
                                <input type="password" name="confirm_new_email" class="form-control" placeholder="Confirm New Email">
                            </div>

                            <div class="pull-right">
                                <button type="submit" class="btn btn-primary">Change Email</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="right-container">
        @include('includes.panels.status')

        @include('includes.panels.top_5_rankings')

    </div>
@endsection