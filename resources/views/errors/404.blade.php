 <!doctype html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <title>ocram</title>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->



    <link rel="stylesheet" href="{{ asset('styles/main.css') }}">
    <link rel="stylesheet" href="{{ asset('styles/style.css') }}">
    <style>
        .__form-group select,
        .__form-group input {
            width: 100%;
        }

        .__form-group select,
        .__form-group input {
            background: #131313;
            border: 1px solid #1d1d1d;
            padding: 10px;
            font: 700 12px Quicksand,san-serif;
            box-shadow: inset 0 0 5px rgba(0,0,0,.3);
            outline: none;
            color: #fff;
        }
    </style>

    <script src="{{ asset('scripts/vendor/modernizr.js') }}"></script>
  </head>
  <body>
    <!--[if IE]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <nav class="__main_navigation_rev">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="list-inline">
                        <li class="item-nav"><a href="{{ route('home') }}">Home</a></li>
                        <li class="item-nav"><a href="{{ route('rankings') }}">Rankings</a></li>
                        <li class="item-nav"><a href="downloads.html">Downloads</a></li>
                        <li class="item-nav"><a href="itemshop.html">Shop</a></li>
                    </ul>
                </div>

                {{-- <div class="col-md-3">
                    @auth
                        <a href="{{ route('auth.logout') }}"><button class="__inverted_btn pull-right">Logout</button></a>
                    @else
                        <a href="registration.html"><button class="__inverted_btn pull-right">Sign Up</button></a>
                    @endif
                </div> --}}
            </div>
        </div>
    </nav>

    <div class="container __main_content">
        <div class="row">
            <div class="col-md-3">
              &nbsp;
            </div>

            <div class="col-md-6 text-center clr__white">

              <div class="__panel">
                <div class="__panel_header">
                    <h3>Error</h3>
                </div>

                <div class="__panel_content">
                                  <h1 style="margin: 0px; font-weight: bolder; text-shadow: 0px 0px 10px rgba(0, 0, 0, 0.4)">404<span class="text-danger">Error</span></h1>
                <small>Page can't be found.</small>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              &nbsp;
            </div>
        </div>
    </div>

    <script src="{{ asset('scripts/vendor.js') }}"></script>

    <script src="{{ asset('scripts/plugins.js') }}"></script>

    <script src="{{ asset('scripts/main.js') }}"></script>
  </body>
  
</html>
