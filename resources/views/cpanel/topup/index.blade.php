@extends('cpanel.layouts.app')

@section('left_content')
    <div class="panel">
        <div class="panel-header">
            Option
        </div>
        <div class="panel-body text-center">
            <a href="{{ route('cpanel.topup.add') }}">
                <button class="btn btn-gradient-primary">Generate New Topup</button>
            </a>
        </div>
    </div>
@endsection

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Top Up
    </div>
    <div class="panel-body">
        <table cellspacing="0">
            <thead>
                <th>Username</th>
                <th>Code</th>
                <th>Pin</th>
                <th>Amount</th>
                <th>Generated</th>
                <th>Option</th>
            </thead>
            <tbody>
                @foreach ($tp as $item)
                    <tr style="{{ $item->used == 1 ? 'color: red;' : '' }}">
                        <td>{{ $user($item->UserNum) }}</td>
                        <td>{{ $item->code }}</td>
                        <td>{{ $item->pin }}</td>
                        <td>{{ $item->value }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td>
                            <ul>
                                <li>
                                    <form id="form-delete" action="{{ route('cpanel.topup.delete') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <button style="background: none; border: none; padding: 0px;" class="text-gold">Delete</button>
                                    </form>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection