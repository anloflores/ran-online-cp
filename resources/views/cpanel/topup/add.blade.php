@extends('cpanel.layouts.app')

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Generate Topup
    </div>
    <div class="panel-body">
        <form action="{{ route('cpanel.topup.store') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="">Username</label>
                <input type="text" name="UserName" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Amount</label>
                <input type="text" name="value" class="form-control">
            </div>

            <button class="btn btn-gradient-primary float-right">Submit</button>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("[name='img']").change(function() {
            readURL(this);
        });
    </script>
@endsection