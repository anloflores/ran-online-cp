@extends('cpanel.layouts.app')

@section('content')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-results__option--selectable {
    cursor: pointer;
    color: #000;
}
    </style>
    <div class="panel">
        <div class="panel-header">
            Send Item to Online Characters Right Now.
        </div>
        <div class="panel-body">
            <form action="{{ route('cpanel.send.online.process') }}" method="POST">
                @csrf
                <div class="form-group" >
                    <label for="">Item</label>
                    <select name="ProductNum" class="items form-control"></select>
                </div>


                <div class="form-group">
                    <label for="">Quantity</label>
                    <input type="number" name="quantity" class="form-control" value="1">
                </div>

                <button class="btn btn-gradient-primary float-right">Send Item</button>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(function() {
            $('.items').select2({
                ajax: {
                    url: '{{ route("cpanel.api.items") }}',
                    dataType: 'json',
                    processResults: function (data) {
                        console.log(data);
                        return {
                            results: data
                        };
                    }
                }
            });
        });
    </script>
@endsection