@extends('cpanel.layouts.app')

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Add Vote Link
    </div>
    <div class="panel-body">
        <form action="{{ route('cpanel.vote.store') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="">Name</label>
                <input type="text" name="name" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Display Image</label><Br/><br/>
                <label for="file" class="btn btn-primary">
                    Select Display Image
                    <input type="file" id="file" name="img" style="display: none;">
                </label>
                <Br/><br/>
                <div id="img-preview">
                    <img src="" alt="" id="preview">
                </div>
                <Br/>
            </div>


            <div class="form-group">
                <label for="">Link</label>
                <input type="text" name="link" class="form-control">
            </div>
            <div style="display: flex; justify-content: space-between;">
                <div class="form-group" style="width: 49%;">
                    <label for="">Reward</label>
                    <input type="text" name="reward" class="form-control">
                </div>
                <div class="form-group" style="width: 49%;">
                    <label for="">Duration (in Hours)</label>
                    <input type="text" name="duration" class="form-control">
                </div>
            </div>

            <button class="btn btn-gradient-primary float-right">Submit</button>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("[name='img']").change(function() {
            readURL(this);
        });
    </script>
@endsection