@extends('cpanel.layouts.app')

@section('left_content')
    <div class="panel">
        <div class="panel-header">
            Option
        </div>
        <div class="panel-body text-center">
            <a href="{{ route('cpanel.vote.add') }}">
                <button class="btn btn-gradient-primary">Add New Link</button>
            </a>
        </div>
    </div>
@endsection

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Vote Links
    </div>
    <div class="panel-body">
        <table cellspacing="0">
            <thead>
                <th>Name</th>
                <th>Link</th>
                <th>Reward</th>
                <th>Duration</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($vote_links as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->link }}</td>
                        <td style="width: 5%;">{{ $item->reward }}</td>
                        <td style="width: 5%;">{{ $item->duration }}</td>
                        <td>
                            <ul>
                                <li><a href="{{ route('cpanel.vote.edit', $item->id) }}">Update</a></li>
                                <li>
                                    <form id="form-delete" action="{{ route('cpanel.vote.delete') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <button style="background: none; border: none; padding: 0px;" class="text-gold">Delete</button>
                                    </form>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection