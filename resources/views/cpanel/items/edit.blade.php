@extends('cpanel.layouts.app')

@section('content')
    <div class="panel">
        <div class="panel-header">
            Update Item #{{$item->ProductNum}}
        </div>
        <div class="panel-body">
            <form action="{{ route('cpanel.items.update', $item->ProductNum) }}" method="POST" enctype="multipart/form-data">
                @csrf
                 @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $i)
                                <li>{{$i}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label for="">Item Name</label>
                    <input type="text" name="ItemName" value="{{ $item->ItemName }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Item Screenshot</label><Br/><br/>
                    <label for="file" class="btn btn-primary">
                        Select File
                        <input type="file" id="file" name="ItemSS" style="display: none;">
                    </label>
                    <Br/><br/>
                    <div id="img-preview">
                        <img src="{{is_file(public_path('items/' . $item->ItemSS)) ? asset('items/' . $item->ItemSS) : asset('items/box.png')}}" alt="" id="preview">
                    </div>
                    <Br/>
                </div>

                <div style="display: flex; justify-content: space-between; width: 100%;">
                    <div class="form-group" style="width: 48%">
                        <label for="">Item Main</label>
                        <input type="text" name="ItemMain" value="{{ $item->ItemMain }}" class="form-control">
                    </div>

                    <div class="form-group" style="width: 48%">
                        <label for="">Item Sub</label>
                        <input type="text" name="ItemSub" value="{{ $item->ItemSub }}" class="form-control">
                    </div>
                </div>

                <div style="display: flex; justify-content: space-between; width: 100%;">
                    <div class="form-group" style="width: 48%">
                        <label for="">Price</label>
                        <input type="text" name="ItemPrice" value="{{ $item->ItemPrice }}" class="form-control">
                    </div>

                    <div class="form-group" style="width: 48%">
                        <label for="">Price Type</label>
                        <select name="ItemSec" id="" class="form-control">
                            <option value="1" {{ $item->ItemSec == 1 ? 'selected' : ''}}>ePoints</option>
                            <option value="2" {{ $item->ItemSec == 2 ? 'selected' : ''}}>vPoints</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Category</label>
                    <select name="ItemCtg" id="" class="form-control">
                        @foreach (\App\Http\Controllers\Helper::$_shopCategories as $k => $i)
                            <option value="{{ $k }}" {{ isset($item->ItemCtg) && $item->ItemCtg == $k ? 'selected' : ''}}>{{ $i }}</option>
                        @endforeach
                    </select>
                </div>
                <div style="display: flex; justify-content: space-between; width: 100%;">
                    <div class="form-group" style="width: 48%">
                        <label for="">Is Item Unlimited?</label>
                        <select name="IsUnli" id="" class="form-control">
                            <option value="1" {{ $item->IsUnli == 1 ? 'selected' : ''}}>Yes</option>
                            <option value="0" {{ $item->IsUnli == 0 ? 'selected' : ''}}>No</option>
                        </select>
                    </div>

                    <div class="form-group" style="width: 48%">
                        <label for="">Item Stocks</label>
                        <input type="number" name="Itemstock" value="{{ $item->Itemstock ?? '0' }}" class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="">Hide Item?</label>
                    <select name="hidden" id="" class="form-control">
                        <option value="0" {{ $item->hidden == 0 ? 'selected' : ''}}>No</option>
                        <option value="1" {{ $item->hidden == 1 ? 'selected' : ''}}>Yes</option>
                    </select>
                </div>

                <button class="btn btn-gradient-primary float-right">Update Item</button>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("[name='ItemSS']").change(function() {
            readURL(this);
        });
    </script>
@endsection