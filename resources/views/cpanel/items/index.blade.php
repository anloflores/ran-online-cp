@extends('cpanel.layouts.app')

@section('left_content')
    <div class="panel">
        <div class="panel-header">
            Options
        </div>
        <div class="panel-body">
            <div class="text-center">
                <a href="{{ route('cpanel.items.add') }}"><button class="btn btn-gradient-primary">Add New Item</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-header">
            Item Shop
        </div>
        <div class="panel-body item-shop-container">
            @foreach ($items as $i)
                <div class="item-shop-item" {!! $i->hidden == 1 ? 'style="opacity: 0.5;"' : '' !!}>
                    <div class="item-shop-item-img">
                        <img src="{{ 
                            is_file(public_path('items/' . $i->ItemSS)) ? asset('items/' . $i->ItemSS) : asset('items/box.png')
                        }}" alt="">
                    </div>

                    <div class="item-shop-item-details">
                        <p>{{ $i->ItemName  }}</p>
                        <p class="text-gold">{{  number_format($i->ItemPrice) }} {{ $i->ItemSec == 1 ? 'ePoints' : 'vPoints'}} {{ $i->hidden == 1 ? ' - Hidden' : '' }}</p>
                        <a href="{{ route('cpanel.items.edit', $i->ProductNum) }}"><button class="btn btn-primary btn-small float-left">Edit</button></a>
                        <button class="btn btn-primary btn-small float-left" data-record="{{ base64_encode(json_encode($i)) }}" data-toggle="modal" data-target="viewItem">VIEW</button>
                        <form action="{{ route('cpanel.items.delete') }}" method="post" class="float-left">
                            @csrf
                            <input type="hidden" name="ProductNum" value="{{ $i->ProductNum }}">
                            <button class="btn btn-primary btn-small" style="background: red; color: #fff;" data-record="{{ base64_encode(json_encode($i)) }}" data-toggle="modal" data-target="viewItem">Delete</button>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

     <div class="modal" id="viewItem">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="font-kanit m-0 float-left" data-column="ItemName">Test</h2>
                <div class="float-right"><i class="fas fa-times" data-modal="close"></i></div>
                <div class="clearfix"></div>
                <hr>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img data-column="ItemSS" src="{{ asset('items/box.png') }}" alt="">
                    <p>Price : <span class="text-gold" data-column="ItemPrice"></span> <span class="text-gold" data-column="ItemSec"></span></p>
                    <p data-column="ItemComment"></p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            $('[data-toggle="modal"]').click(function() {
                var d = JSON.parse(atob($(this).data('record')));
                $('#viewItem').find('[data-column]').each((i, e) => {
                    var col = $(e).data('column');

                    if(col == 'ItemSS') {
                        $(e).attr('src', (d[col] == '' || d[col] == null ? '{{ asset("items/box.png") }}' : '{{ asset("items") }}/' + d[col]))
                    } else if(col == 'ItemSec') {
                        $(e).text(d[col] == 1 ? 'ePoints' : 'vPoints');
                    } else {
                        $(e).text(d[col]);
                    }
                })
            })
        })
    </script>
@endsection