@extends('cpanel.layouts.app')

@section('content')
    <div class="panel">
        <div class="panel-header">
            Give ePoints
        </div>
        <div class="panel-body">
            <div style="width: 50%;">
               <form action="{{ route('cpanel.give.process') }}" method="POST">
                   @csrf
                   <input type="hidden" name="type" value="1">
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">ePoints Amount</label>
                        <input type="integer" name="amount" class="form-control">
                    </div>

                    <button class="btn btn-gradient-primary float-right">Give</button>
                    <div class="clearfix"></div>
               </form>
            </div>
        </div>
    </div>
@endsection