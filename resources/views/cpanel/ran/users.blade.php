@extends('cpanel.layouts.app')

@section('content')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-results__option--selectable {
            cursor: pointer;
            color: #000;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
    </style>

        <div class="panel">
            <div class="panel-header">
                Search Username
            </div>
            <div class="panel-body">
                <form action="" method="GET">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Username</label>
                                <select name="UserName" class="username form-control"></select>
                            </div>



                <button class="btn btn-gradient-primary float-right">View User</button>
                <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @isset($user)
            <div class="panel">
                <div class="panel-header">
                    {{ $user->UserNum }} - {{ $user->UserName}}
                </div>

                <div class="panel-body">
                    <table>
                        <tr>
                            <td>ePoints : <span class="text-gold">{{ $points->ePoints ?? 0 }}</span></td>
                            <td>vPoints : <span class="text-gold">{{ $points->vPoints ?? 0 }}</span> </td>
                        </tr>
                    </table>
                </div>
            </div>
        @endisset

        @isset($characters)
            <h1>Characters</h1>
            @foreach ($characters as $c)
                <div class="panel __lower_margin">
                    <div class="panel-header">
                        <div>
                            <img style="height: 30px; width: 30px; border-radius: 50%; margin-right: 20px;" class="float-left" src="{{ asset('assets/img/class/' . $c->ChaClass . '.jpg') }}" alt="">
                            <div class="float-left small" style="margin-left: 0px; margin-top: -2px;">
                                <div class="clr__white">{{ $c->ChaName }}</div>
                                @if ($c->GuNum)
                                    <div class="clr__white"><img src="{{ route('gu_badge', $c->GuNum) }}" alt="">{{ $c->ChaGuName }}</div>
                                @endif
                            </div>

                            <div class="float-right">
                                <div class="label label-{{ $c->ChaOnline == 1 ? 'success' : 'danger' }} small float-right" style="height: 12px;
                                width: 12px;
                                border-radius: 50% !important;
                                padding: 0px;
                                box-shadow: 0px 0px 10px rgba(0, 0,0, 0.1);" title="{{ $c->ChaOnline == 1 ? 'Online' : 'Offline'}}">&nbsp;</div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div>
                            <br>
                            <p class="clr__white">
                                <h4 class="title font__pd clr__white">Details</h4>
                                <table class="table small" style="width: 100%;">
                                    <tr>
                                        <td class="text-gold">HP</td>
                                        <td class="clr__white">{{ number_format($c->ChaHP) }}</td>
                                        <td class="text-gold">MP</td>
                                        <td class="clr__white">{{ number_format($c->ChaMP) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-gold">Level</td>
                                        <td class="clr__white">{{ $c->ChaLevel }}</td>
                                        <td class="text-gold">Class</td>
                                        <td class="clr__white">{{ App\Http\Controllers\Helper::_getClass($c->ChaClass) }}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            $school = App\Http\Controllers\Helper::_getSchool($c->ChaSchool);
                                        @endphp
                                        <td class="text-gold">School</td>
                                        <td class="clr__white">{{ $school }} <img src="{{ asset('images/school/mini/' . $school . '.png') }}" alt=""></td>
                                        <td class="text-gold">Gold</td>
                                        <td class="clr__white">{{ number_format($c->ChaMoney) }}</td>
                                    </tr>
                                </table>
                                <h4 class="title font__pd clr__white">PK</h4>
                                <table class="table small" style="width: 100%;">
                                    <tr>
                                        <td class="text-gold">Win</td>
                                        <td class="clr__white">{{ number_format($c->ChaPkWin) }}</td>
                                        <td class="text-gold">Loss</td>
                                        <td class="clr__white">{{ number_format($c->ChaPkLoss) }}</td>
                                    </tr>
                                </table>
                                <h4 class="title font__pd clr__white">Stats</h4>
                                <table class="table small" style="width: 100%;">
                                    <tr>
                                        <td class="text-gold">Pow</td>
                                        <td class="clr__white">{{ number_format($c->ChaPower) }}</td>
                                        <td class="text-gold">Dex</td>
                                        <td class="clr__white">{{ number_format($c->ChaDex) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-gold">Int</td>
                                        <td class="clr__white">{{ number_format($c->ChaSpirit) }}</td>
                                        <td class="text-gold">Stm</td>
                                        <td class="clr__white">{{ number_format($c->ChaStrength) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-gold">Vit</td>
                                        <td class="clr__white">{{ number_format($c->ChaStrong) }}</td>
                                        <td class="text-gold">Free</td>
                                        <td class="clr__white">{{ number_format($c->ChaStRemain) }}</td>
                                    </tr>
                                </table>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="text-center clr__white">
                <h1>No Character Found</h1>
            </div>
        @endisset

        @isset($logs)
            <div class="panel">
                <div class="panel-header">
                    Logs
                </div>
                <div class="panel-body">
                    <table cellspacing="0">
                        <thead class="logs">
                            <th>Message</th>
                            <th>Date</th>
                        </thead>
                        <tbody>
                            @foreach ($logs as $item)
                                <tr>
                                    <td>{{ $item->message }}</td>
                                    <td>{{ date('y-m-d H:i', strtotime($item->created_at)) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endisset
@endsection



@section('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(function() {
            $('.username').select2({
                ajax: {
                    url: '{{ route("cpanel.api.username") }}',
                    dataType: 'json',
                    processResults: function (data) {
                        console.log(data);
                        return {
                            results: data
                        };
                    }
                }
            });

            $('.items').select2({
                ajax: {
                    url: '{{ route("cpanel.api.items") }}',
                    dataType: 'json',
                    processResults: function (data) {
                        console.log(data);
                        return {
                            results: data
                        };
                    }
                }
            });
        });
    </script>
@endsection