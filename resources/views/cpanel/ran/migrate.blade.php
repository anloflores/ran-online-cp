@extends('cpanel.layouts.app')

@section('content')
    <div class="panel">
        <div class="panel-header">
            Migrate Points From SeiferCP
        </div>
        <div class="panel-body">
            @if ($check != null)
                <form action="{{ route('cpanel.migrate.seifer.process') }}" method="POST">
                    @csrf
                    <div class="">
                        ** By Clicking Migrate, the system will automatically migrate the ePoints and vPoints from
                        SeiferCP to current CP.<br/><br/>
                        All ePoints and vPoints on SeiferCP will be set to 0, in order to prevent duplicate migration.
                        <br/><br/>
                        Please be sure if you're going to continue. This action is irrevesible.
                        <br/><br/>
                        Total Records to Migrate : {{ count($check) }}

                        <br>
                        Some records can't be migrated. Either the Users from points doesn't exists on the RanUser table.
                    </div>
                    <Br/><br/>
                    <div class="text-center">
                        <button class="btn btn-gradient-primary text-center">Migrate</button>
                    </div>
                </form>
            @else
                <div class="text-center">
                    No SeiferCP Database Located on your server.
                </div>
            @endif
        </div>
    </div>
@endsection