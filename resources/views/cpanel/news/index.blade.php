@extends('cpanel.layouts.app')

@section('left_content')
    <div class="panel">
        <div class="panel-header">
            Option
        </div>
        <div class="panel-body">
            <div class="text-center">
                <a href="{{ route('cpanel.news.create') }}"><button class="btn btn-primary btn-sm pull-right">Add News</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
<style>
    table {
        width: 100%;
        border-collapse:separate; 
        border-spacing:0 5px; 
    }

    table thead th {
        background: #2f2f2f;
        border: 10px;
        padding: 10px;
        font-size: 12px;
        text-align: left;
    }

    table tbody td {
        background: #2f2f2f;
        border: 10px;
        padding: 10px;
        font-size: 12px;
    }

</style>
        <div class="panel">
            <div class="panel-header">
                News
            </div>
            <div class="panel-body">
                <table>
                    <thead class="">
                        <th>ID</th>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Option</th>
                    </thead>

                    <tbody>
                        @foreach ($news as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    @switch($item->type)
                                        @case(1)
                                            News
                                            @break
                                        @case(2)
                                            Update
                                            @break
                                        @case(3)
                                            Hot
                                            @break
                                        @case(5)
                                            Download
                                            @break
                                            
                                    @endswitch
                                </td>
                                <td style="width: 27%;" class="text-center">
                                    <a href="{{ route('news', $item->id) }}" target="_blank">View</a> -
                                    <a href="{{ route('cpanel.news.edit', $item->id) }}">Update</a> -
                                    <a href="#">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                        @if (count($news) <= 0)
                            <tr>
                                <td colspan="4" class="text-center">No News added yet.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
@endsection