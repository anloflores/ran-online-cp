@extends('cpanel.layouts.app')

@section('content')
<div class="row">
    <div class="col-md-9">
        <div class="clearfix"></div>
        <div class="panel">
            <div class="panel-header">
                Add News
            </div>
            <div class="panel-body">
                <form action="{{ route('cpanel.news.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Type</label>
                                <select name="type" id="" class="form-control">
                                    <option value="">-- Select Type --</option>
                                    <option value="1">News</option>
                                    <option value="2">Update</option>
                                    <option value="3">Hot</option>
                                    <option value="5">Download Page</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Content</label><br/>
                            <textarea name="content" id="content" cols="30" rows="10"></textarea>
                        </div>

                         <div class="col-md-12">
                            <div class="pull-right">
                                <br/>
                                <button type="submit" class="btn btn-primary float-right">Add News</button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6"></div>
</div>
@endsection

@section('script')
    <script src="https://cdn.tiny.cloud/1/j8lijrm2e700t2dwequm2ith2t1ab3mpj6sjgwr309yzw1rv/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
        selector: '#content',
            plugins: 'lists media table code',
            toolbar: 'addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table code',
            toolbar_drawer: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
            content_style: 'body { font-family: Quicksand, sans-serif; font-size: 14px; }'
        });
    </script>
@endsection