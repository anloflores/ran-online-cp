@extends('cpanel.layouts.app')

@section('content')
<div class="row">
    <div class="col-md-9">
        <h4 class="title font__pd clr__white pull-left">Update News</h4>
        <div class="clearfix"></div>
        <div class="__panel">
            <div class="__panel_content">
                <form action="{{ route('cpanel.news.update', $news->id) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8">
                            <div class="__form-group">
                                <label for="">Title</label>
                                <input type="text" name="title" placeholder="Title" value="{{ $news->title }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="__form-group">
                                <label for="">Type</label>
                                <select name="type" id="" class="form-control">
                                    <option value="">-- Select Type --</option>
                                    <option value="1" {{ $news->type == 1 ? 'selected' : '' }}>News</option>
                                    <option value="2" {{ $news->type == 2 ? 'selected' : '' }}>Update</option>
                                    <option value="3" {{ $news->type == 3 ? 'selected' : '' }}>Hot</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <textarea name="content" id="content" cols="30" rows="10">{{ $news->content }}</textarea>
                        </div>

                         <div class="col-md-12">
                            <div class="pull-right">
                                <br/>
                                <button type="submit" class="__main_btn">Update News</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6"></div>
</div>
@endsection

@section('script')
    <script src="https://cdn.tiny.cloud/1/j8lijrm2e700t2dwequm2ith2t1ab3mpj6sjgwr309yzw1rv/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
        selector: '#content',
            plugins: 'linkchecker lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
            toolbar_drawer: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
            content_style: 'body { font-family: Quicksand, sans-serif; font-size: 14px; }'
        });
    </script>
@endsection