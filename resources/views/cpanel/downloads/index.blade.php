@extends('cpanel.layouts.app')

@section('content')
<style>
    table {
        width: 100%;
        border-collapse:separate; 
        border-spacing:0 5px; 
    }

    table thead th {
        background: #2f2f2f;
        border: 10px;
        padding: 10px;
        font-size: 12px;
    }

    table thead th:first-child {
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }

    table thead th:last-child {
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
    }

    table tbody td {
        background: #fff;
        border: 10px;
        padding: 10px;
        font-size: 12px;
    }

    table tbody td:first-child {
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }

    table tbody td:last-child {
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
    }
</style>
<div class="row">
    <div class="col-md-9">
        <h4 class="title font__pd clr__white pull-left">Downloads</h4>
        <a href="{{ route('cpanel.news.create') }}"><button class="btn btn-primary btn-sm pull-right">Add Download Link</button></a>
        <div class="clearfix"></div>
        <div class="__panel">
            <div class="__panel_content">
                <table>
                    <thead class="clr__white">
                        <th>ID</th>
                        <th>Provider</th>
                        <th>Type</th>
                        <th>Size</th>
                        <th>Link</th>
                        <th>Option</th>
                    </thead>

                    <tbody>
                        @foreach ($news as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->provider }}</td>
                                <td>{{ $item->type == 1 ? 'Full Client' : 'Patch' }}</td>
                                <td>{{ $item->size }}</td>
                                <td>{{ $item->link }}</td>
                                <td style="width: 27%;" class="text-center">
                                    <a href="#"><div class="label label-success">View</div></a>
                                    <a href="{{ route('cpanel.news.edit', $item->id) }}"><div class="label label-warning">Update</div></a>
                                    <a href="#"><div class="label label-danger">Delete</div></a>
                                </td>
                            </tr>
                        @endforeach

                        @if (count($news) <= 0)
                            <tr>
                                <td colspan="6" class="text-center">No Download added yet.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6"></div>
</div>
@endsection