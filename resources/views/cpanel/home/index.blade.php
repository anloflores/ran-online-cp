@extends('cpanel.layouts.app')

@section('content')
<div class="row">
    <div class="col-md-9">
        <div class="panel">
            <div class="panel-header">
                Statistics
            </div>
            <div class="panel-body">
                <h4 class="title font__pd clr__white pull-left">Online</h4>
                <div class="pull-right text-center">
                    <h4 class="text-green">{{ number_format($characters->online) ?? 0 }}</h4>
                </div>
                <div class="clearfix"></div>

                <h4 class="title font__pd clr__white pull-left">Accounts</h4>
                <div class="pull-right text-center text-green">
                    <h4 class="text-success">{{ number_format($accounts->total) }}</h4>
                </div>
                <div class="clearfix"></div>
                <h4 class="title font__pd clr__white">Characters</h4>
                <div class="row" style="display: flex; justify-content: space-evenly;">
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->brawler) ?? 0 }}</h4>
                        <small class="text-warning">Brawler</small>
                    </div>
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->swordsman) ?? 0 }}</h4>
                        <small class="text-warning">Swordsman</small>
                    </div>
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->archer) ?? 0 }}</h4>
                        <small class="text-warning">Archer</small>
                    </div>
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->shaman) ?? 0 }}</h4>
                        <small class="text-warning">Shaman</small>
                    </div>
                </div>
                <hr>
                <div class="row" style="display: flex; justify-content: space-evenly;">
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->gunner) ?? 0 }}</h4>
                        <small class="text-warning">Gunner</small>
                    </div>
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->assassin) ?? 0 }}</h4>
                        <small class="text-warning">Assassin</small>
                    </div>
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->magician) ?? 0 }}</h4>
                        <small class="text-warning">Magician</small>
                    </div>
                 </div>
                 <hr>
                <div class="row" style="display: flex; justify-content: space-evenly;">
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->total) ?? 0 }}</h4>
                        <small class="text-warning">Total</small>
                    </div>
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->deleted) ?? 0 }}</h4>
                        <small class="text-danger">Deleted</small>
                    </div>
                    <div class="col-md-3 text-center">
                        <h4 class="text-gold" style="margin: 0px !important;">{{ number_format($characters->active) ?? 0 }}</h4>
                        <small class="text-success">Total Active</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6"></div>
</div>
@endsection