@extends('cpanel.layouts.app')

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Settings
    </div>
    <div class="panel-body">
        <form action="{{ route('cpanel.settings.update') }}" method="POST">
            @csrf
            
            <div class="form-group">
                <label for="">Ran Name</label>
                <input type="text" name="title" value="{{ $_settings('title') }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Ran Description</label>
                <input type="text" name="description" value="{{ $_settings('description') }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Online Multiplier</label>
                <input type="text" name="online_multiplier" value="{{ $_settings('online_multiplier') }}" class="form-control">
            </div>
            <h3>Gametime to Vote Points</h3>
            <div class="form-group">
                <label for="">Gametime Worth</label>
                <input type="text" name="gt2vp.worth" value="{{ $_settings('gt2vp')['conversion'][0] }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="">VPoints Equivalent</label>
                <input type="text" name="gt2vp.equivalent" value="{{ $_settings('gt2vp')['conversion'][1] }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="">GameTime Column</label>
                <input type="text" name="col.gametime" value="{{ $_settings('columns')['GameTime'] }}" class="form-control">
            </div>

            <h3>Vote Settings</h3>
            <div class="form-group">
                <label for="">Vote Level Requirements</label>
                <input type="text" name="vote.level_req" value="{{ $_settings('voting')['level_req'] }}" class="form-control">
            </div>
            
            <button class="btn btn-gradient-primary float-right">Update Settings</button>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection