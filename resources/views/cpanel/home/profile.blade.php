@extends('cpanel.layouts.app')

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Update Profile
    </div>
    <div class="panel-body">
        <form action="{{ route('cpanel.profile.update') }}" method="POST">
            @csrf
            
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" name="name" class="form-control" value="{{ Auth::guard('admin')->user()->name }}">
            </div>
            <div class="form-group">
                <label for="">email</label>
                <input type="text" name="email" class="form-control" value="{{ Auth::guard('admin')->user()->email }}">
            </div>
            <div class="form-group">
                <label for="">password</label>
                <input type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="">confirm password</label>
                <input type="password" name="repassword" class="form-control" >
            </div>

            <div class="form-group">
                <label for="">current password to confirm update</label>
                <input type="password" name="current_password" class="form-control" >
            </div>

            <button class="btn btn-gradient-primary float-right">Update Profile</button>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection