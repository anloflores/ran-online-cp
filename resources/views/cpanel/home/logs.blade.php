@extends('cpanel.layouts.app')

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Logs
    </div>
    <div class="panel-body">
        <table cellspacing="0">
            <thead>
                <th>UserNum</th>
                <th>User Type</th>
                <th>Action</th>
                <th>Message</th>
                <th>Date</th>
            </thead>
            <tbody>
                @foreach ($logs as $item)
                    <tr>
                        <td>{{ $item->UserNum }}</td>
                        <td>{{ $item->type == 1 ? 'Admin' : 'User' }}</td>
                        <td>{{ $item->action }}</td>
                        <td>{{ $item->message }}</td>
                        <td>{{ date('y-m-d H:i', strtotime($item->created_at)) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="center">
            {{ $logs->links() }}
        </div>
    </div>
</div>
@endsection