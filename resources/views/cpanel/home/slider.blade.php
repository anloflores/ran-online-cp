@extends('cpanel.layouts.app')

@section('content')
<style>
    table {
        width: 100%;
    }
    th,td {
        border: 0.5px solid #272727;
        padding: 5px;
    }
</style>
<div class="panel">
    <div class="panel-header">
        Image Slider
    </div>
    <div class="panel-body">
        <form action="{{ route('cpanel.slider.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
             <div class="form-group">
                <label for="file" class="btn btn-primary float-left" style="margin-top: 0px;">
                    Select File
                    <input type="file" id="file" name="image" style="display: none;">
                </label>
                
                <button class="btn btn-gradient-primary float-right">Upload</button>
                <div class="clearfix"></div>
            </div>

            <div class="form-group">
                <label for="">Preview</label>
                <div id="img-preview">
                    <img src="" alt="" id="preview">
                </div>
                <hr>
            </div>
        </form>

        <div class="form-group">
            <label for="">Uploaded Images</label>
        </div>

        <div>
            @foreach ($imgs as $i)
              
                <div class="text-center" style="position: relative;">
                      <img src="{{ asset('slider/' . $i->name) }}" alt="">
                    <form action="{{ route('cpanel.slider.delete') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $i->id }}">
                        <button class="btn btn-primary" style="background: red; color: white; font-size: 10px; position: absolute; bottom: 20px;">Delete</button>
                    </form>
                </div>
                <hr>
            @endforeach
        </div>

    </div>
</div>
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("[name='image']").change(function() {
            readURL(this);
        });
    </script>
@endsection