<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('ran.title') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fontawesome/css/all.min.css') }}">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <style>
        .menu li {
            padding: 3px 0px;
            text-transform: uppercase;
            font-family: 'Kanit', sans-serif;
        }
    </style>
</head>
<body  class="adminpage">
    <div class="container">
        <h1>Admin Control Panel</h1>
    </div>
    <div class="container" style="justify-content: center;">
         <div class="admin-left-container">
            <div class="panel">
                <div class="panel-header">
                    Menu
                </div>
                <div class="panel-body">
                    <ul class="menu" style="margin:0; padding-left: 15px;">
                        <li><a href="{{ route('cpanel.home') }}" class="list-group-item">Statistics</a><br/></li>
                        <li><a href="{{ route('cpanel.news.index') }}" class="list-group-item">News</a><br/></li>
                        <li><a href="{{ route('cpanel.slider') }}" class="list-group-item">Image Slider</a><br/></li>
                        <li><a href="{{ route('cpanel.items.index') }}" class="list-group-item">Item Shop</a><br/></li>
                        <li><a href="{{ route('cpanel.vote.index') }}" class="list-group-item">Vote Links</a><br/></li>
                        <li style="list-style: none;"><hr></li>
                        <li><a href="{{ route('cpanel.users') }}" class="list-group-item">Users</a><br/></li>
                        <li><a href="{{ route('cpanel.characters') }}" class="list-group-item">Characters</a><br/></li>
                        <li style="list-style: none;"><hr></li>
                        <li><a href="{{ route('cpanel.topup.index') }}" class="list-group-item">Top Up</a><br/></li>
                        <li><a href="{{ route('cpanel.give.epoints') }}" class="list-group-item">Give ePoints</a><br/></li>
                        <li><a href="{{ route('cpanel.give.vpoints') }}" class="list-group-item">Give vPoints</a><br/></li>
                        <li style="list-style: none;"><hr></li>
                        <li><a href="{{ route('cpanel.send.character') }}" class="list-group-item">Send Item to Character</a><br/></li>
                        <li><a href="{{ route('cpanel.send.username') }}" class="list-group-item">Send Item to Username</a><br/></li>
                        <li><a href="{{ route('cpanel.send.attendance') }}" class="list-group-item">Send Item to Attendance</a><br/></li>
                        <li><a href="{{ route('cpanel.send.online') }}" class="list-group-item">Send Item to Online</a><br/></li>
                        <li><a href="{{ route('cpanel.send.all') }}" class="list-group-item">Send Item to All Users</a><br/></li>
                        <li style="list-style: none;"><hr></li>
                        <li><a href="{{ route('cpanel.profile') }}" class="list-group-item">Profile</a><br/></li>
                        <li><a href="{{ route('cpanel.settings') }}" class="list-group-item">Settings</a><br/></li>
                        <li style="list-style: none;"><hr></li>
                        <li><a href="{{ route('cpanel.logs') }}" class="list-group-item">Logs</a><br/></li>
                        <li style="list-style: none;"><hr></li>
                        <li><a href="{{ route('cpanel.migrate.seifer') }}" class="list-group-item">Migrate Points (SeiferCP)</a><br/></li>
                        <li><a href="{{ route('cpanel.cache') }}" class="list-group-item">Delete Cache</a><br/></li>
                    </ul>
                </div>
            </div>

            @yield('left_content')
        </div>

        <div class="admin-container">
            @yield('content')
        </div>
    </div>
</body>

<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@if (session('success') !== null)
    @if(session('success') == true)
        <script>
            toastr.success("{{ session('msg') ?? 'Action Success' }}")
        </script>
    @endif

    @if(session('success') == false)
        <script>
            toastr.error("{{ session('msg') ?? 'Action Success' }}")
        </script>
    @endif

    
@endif

 @if ($errors->any())
        @foreach ($errors->all() as $item)
            <script>
                toastr.error("{{ $item }}")
            </script>
        @endforeach
    @endif
<script>
    $(function() {
        $('[data-toggle="modal"]').click(function() {
            var target = $(this).data('target');
            $('#' + target).fadeIn("fast");
        });

        $('[data-modal="close"]').click(function() {
            $('.modal').fadeOut();
        })
    })
</script>
@yield('script')
</html>