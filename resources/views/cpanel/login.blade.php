<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prestige Ran Online</title>

    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fontawesome/css/all.min.css') }}">
</head>
<body  class="adminpage">
    <div class="container" style="justify-content: center;">
        <div class="admin-left-container" style="margin-top: 100px;">
            <div class="panel">
                <div class="panel-header">
                    CPanel Login
                </div>
                <div class="panel-body">
                    <div class="text-center">
                        @if (session('msg') != null)
                            {{ session('msg') }}
                            <Br/>
                        @endif
                    </div>
                     <form action="{{ route('cpanel.auth.login') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Username</label>
                            <input name="email" type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Password</label>
                            <input name="password" type="password" class="form-control">
                        </div>

                        <div class="form-group float-left" style="margin-top: 10px;">
                            <!-- <a href="#">Forgot Password?</a> -->
                        </div>

                        <div class="form-group float-right">
                            <button class="btn btn-gradient-primary">Login</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="assets/js/jquery.min.js"></script>
<script>
    //Slider
        var speed = 5000; // milisecond
        var current = $('.slider-items > li.active').index();
        var max = $('.slider-items > li').length-1;

        var slider = () => {
            if(current == max) {
                current = 0;
            } else {
                current++;
            }

            $('.slider-items > li').removeClass('active');
            $('.slider-items > li').eq(current).addClass('active');
        }

        var interval = setInterval(function() {
            slider()
        }, speed)

        // Slider Control

        $('.slider-control > .prev').click(function() {

            clearInterval(interval);

            if(current == 0) {
                current = max;
            } else {
                current--;
            }

            $('.slider-items > li').removeClass('active');
            $('.slider-items > li').eq(current).addClass('active');

            interval = setInterval(function() {
                slider()
            }, speed)
        });

        $('.slider-control > .next').click(function() {

            clearInterval(interval);

            if(current == max) {
                current = 0;
            } else {
                current++;
            }

            $('.slider-items > li').removeClass('active');
            $('.slider-items > li').eq(current).addClass('active');

            interval = setInterval(function() {
                slider()
            }, speed)
        });
</script>
</html>