<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

// Route::get('/test', 'TestController@test');
// Route::get('/test/str', 'TestController@itemstr');

Route::get('/home', 'HomeController@index')->name('login');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/news/{id}', 'HomeController@news')->name('news');

Route::get('/rankings', 'RankController@index')->name('rankings');
Route::get('/downloads', 'HomeController@downloads')->name('downloads');

Route::get('/register', 'UserController@register')->name('register');
Route::post('/register', 'UserController@register_process')->name('register.process');

Route::post('/auth', 'AuthController@login')->name('auth');
Route::get('/logout', 'AuthController@logout')->name('auth.logout');


Route::get('/gu/image/{id}', 'RankController@guild_badge')->name('gu_badge');

Route::group(['middleware' => ['auth', 'web']], function() {
    Route::get('/ran/characters', 'RanController@characters')->name('characters');
    Route::get('/shop', 'ShopController@index')->name('shop');
    Route::post('/shop/purchase', 'ShopController@purchase')->name('shop.buy.process');

    Route::get('/change/password', 'UserController@change_password')->name('change_password');
    Route::post('/change/password', 'UserController@change_password_process')->name('change_password.process');

    Route::get('/change/email', 'UserController@change_email')->name('change_email');
    Route::post('/change/email', 'UserController@change_email_process')->name('change_email.process');

    Route::get('/ran/change/school', 'RanController@change_school')->name('change_school');
    Route::post('/ran/change/school', 'RanController@change_school_process')->name('change_school.process');

    Route::get('/ran/conversion/gt2vp', 'RanController@gt_to_vp')->name('gt_to_vp');
    Route::post('/ran/conversion/gt2vp', 'RanController@gt_to_vp_process')->name('gt_to_vp.process');

    Route::get('/ran/vote', 'VoteController@index')->name('vote');
    Route::post('/ran/vote/store', 'VoteController@store')->name('vote.store');

    Route::get('/ran/topup', 'TopUpController@index')->name('topup');
    Route::post('/ran/topup/store', 'TopUpController@store')->name('topup.store');
});

Route::get('/cpanel', 'CPanel\AuthController@login')->name('cpanel.login');
Route::post('/cpanel', 'CPanel\AuthController@login_process')->name('cpanel.auth.login');

Route::group(['prefix' => '/cpanel', 'middleware' => ['admin']], function() {
    Route::get('/home', 'CPanel\HomeController@index')->name('cpanel.home');
    Route::get('/logs', 'CPanel\HomeController@logs')->name('cpanel.logs');
    Route::get('/characters', 'CPanel\RanController@characters')->name('cpanel.characters');
    Route::get('/users', 'CPanel\RanController@users')->name('cpanel.users');
    Route::get('/delete/cache', 'CPanel\HomeController@cache')->name('cpanel.cache');

    Route::get('/give/epoints', 'CPanel\RanController@give_epoints')->name('cpanel.give.epoints');
    Route::get('/give/vpoints', 'CPanel\RanController@give_vpoints')->name('cpanel.give.vpoints');
    Route::get('/give/gold', 'CPanel\RanController@give_gold')->name('cpanel.give.gold');
    Route::post('/give/process', 'CPanel\RanController@give_process')->name('cpanel.give.process');

    Route::get('/news', 'CPanel\NewsController@index')->name('cpanel.news.index');
    Route::get('/news/create', 'CPanel\NewsController@create')->name('cpanel.news.create');
    Route::post('/news/create', 'CPanel\NewsController@store')->name('cpanel.news.store');
    Route::get('/news/{id}/edit', 'CPanel\NewsController@edit')->name('cpanel.news.edit');
    Route::post('/news/{id}/edit', 'CPanel\NewsController@update')->name('cpanel.news.update');

    Route::get('/item', 'CPanel\ItemShopController@index')->name('cpanel.items.index');
    Route::get('/item/add', 'CPanel\ItemShopController@add')->name('cpanel.items.add');
    Route::post('/item/store', 'CPanel\ItemShopController@store')->name('cpanel.items.store');
    Route::get('/item/{id}/edit', 'CPanel\ItemShopController@edit')->name('cpanel.items.edit');
    Route::post('/item/{id}/update', 'CPanel\ItemShopController@update')->name('cpanel.items.update');
    Route::post('/item/delete', 'CPanel\ItemShopController@delete')->name('cpanel.items.delete');

    // Vote
    Route::get('/vote', 'CPanel\VoteController@index')->name('cpanel.vote.index');
    Route::get('/vote/add', 'CPanel\VoteController@add')->name('cpanel.vote.add');
    Route::post('/vote/store', 'CPanel\VoteController@store')->name('cpanel.vote.store');
    Route::get('/vote/{id}/edit', 'CPanel\VoteController@edit')->name('cpanel.vote.edit');
    Route::post('/vote/update', 'CPanel\VoteController@update')->name('cpanel.vote.update');
    Route::post('/vote/delete', 'CPanel\VoteController@delete')->name('cpanel.vote.delete');

    // Topup
    Route::get('/topup', 'CPanel\TopUpController@index')->name('cpanel.topup.index');
    Route::get('/topup/add', 'CPanel\TopUpController@add')->name('cpanel.topup.add');
    Route::post('/topup/store', 'CPanel\TopUpController@store')->name('cpanel.topup.store');
    Route::post('/topup/delete', 'CPanel\TopUpController@delete')->name('cpanel.topup.delete');

    // send item
        // Character
        Route::get('/send/character', 'CPanel\SendItemController@character')->name('cpanel.send.character');
        Route::post('/send/character/process', 'CPanel\SendItemController@character_process')->name('cpanel.send.character.process');
        // Username
        Route::get('/send/username', 'CPanel\SendItemController@username')->name('cpanel.send.username');
        Route::post('/send/username/process', 'CPanel\SendItemController@username_process')->name('cpanel.send.username.process');
        // Attendance
        Route::get('/send/attendance', 'CPanel\SendItemController@attendance')->name('cpanel.send.attendance');
        Route::post('/send/attendance/process', 'CPanel\SendItemController@attendance_process')->name('cpanel.send.attendance.process');
        // Online
        Route::get('/send/online', 'CPanel\SendItemController@online')->name('cpanel.send.online');
        Route::post('/send/online/process', 'CPanel\SendItemController@online_process')->name('cpanel.send.online.process');
        // Online
        Route::get('/send/all', 'CPanel\SendItemController@all')->name('cpanel.send.all');
        Route::post('/send/all/process', 'CPanel\SendItemController@all_process')->name('cpanel.send.all.process');


    // Migration
    Route::get('/migrate/seifer', 'CPanel\MigrationController@seifer_migrate')->name('cpanel.migrate.seifer');
    Route::post('/migrate/seifer', 'CPanel\MigrationController@seifer_migrate_all_process')->name('cpanel.migrate.seifer.process');
    
    // Profile
    Route::get('/profile', 'CPanel\UserController@index')->name('cpanel.profile');
    Route::post('/profile/update', 'CPanel\UserController@update')->name('cpanel.profile.update');

    // Settings
    Route::get('/settings', 'CPanel\SettingsController@index')->name('cpanel.settings');
    Route::post('/settings/update', 'CPanel\SettingsController@update')->name('cpanel.settings.update');

    // Images
    Route::get('/slider', 'CPanel\ImagesController@index')->name('cpanel.slider');
    Route::post('/slider/store', 'CPanel\ImagesController@store')->name('cpanel.slider.store');
    Route::post('/slider/delete', 'CPanel\ImagesController@delete')->name('cpanel.slider.delete');


    // APIS
    Route::get('/api/character/search', function(Request $request) {
        $data = DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->select([
                'ChaName as id',
                'ChaName as text'
            ])
            ->where('ChaName', 'LIKE', '%'. $request->term .'%')
            ->get();

        return response()->json($data, 200);

    })->name('cpanel.api.characters');
    
    Route::get('/api/items/search', function(Request $request) {
        $data = DB::connection('RanShop')
            ->table('dbo.ShopItemMap')
            ->select([
                'ProductNum as id',
                'ItemName as text'
            ])
            ->where('ItemName', 'LIKE', '%'. $request->term .'%')
            ->where('ItemName', '!=', '')
            ->get();

        return response()->json($data, 200);

    })->name('cpanel.api.items');
    
    Route::get('/api/username/search', function(Request $request) {
        $data = DB::connection('RanUser')
            ->table('dbo.UserInfo')
            ->select([
                'UserName as id',
                'UserName as text'
            ])
            ->where('UserName', 'LIKE', '%'. $request->term .'%')
            ->get();

        return response()->json($data, 200);

    })->name('cpanel.api.username');
});