<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Helper;

use DB;

class RankController extends Controller
{
    public function index(Request $request) {
        
        $class = strtolower($request->class) ?? null;

        if($class != null && !Helper::$_activeClasses[$class])
            return response()->json([], 404);

        if($request->page > Helper::$_maxValues['rankingsPage'])
            return response()->json([], 404);

        $params = implode('--', $request->all());
        $topCharacters = Cache::remember('topCharacters--' . $params, Helper::$_cacheTime['topPlayer'], function () use ($class, $request) {
            $top = DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->selectRaw("
                RanGame1.dbo.ChaInfo.ChaName as ChaName, 
                RanGame1.dbo.GuildInfo.GuName as ChaGuName, 
                (CASE
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(1,64) THEN 'Brawler'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(2,128) THEN 'Swordsman'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(4,256) THEN 'Archer'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(8,512) THEN 'Shaman'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(1024,2048) THEN 'Gunner'
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(4096,8192) THEN 'Assassin' 
                    WHEN RanGame1.dbo.ChaInfo.ChaClass IN(16384,32768) THEN 'Magician'
                END) as ChaClass, 
                RanGame1.dbo.ChaInfo.ChaClass as ChaClassNum,
                (CASE 
                    WHEN RanGame1.dbo.ChaInfo.ChaSchool = 0 THEN 'SG'
                    WHEN RanGame1.dbo.ChaInfo.ChaSchool = 1 THEN 'MP'
                    WHEN RanGame1.dbo.ChaInfo.ChaSchool = 2 THEN 'PHX'
                    WHEN ChaSchool NOT IN(0, 1, 2) THEN 'Leonair'
                END) as ChaSchool, 
                RanGame1.dbo.ChaInfo.ChaLevel as ChaLevel, 
                RanGame1.dbo.ChaInfo.ChaMoney as ChaMoney, 
                RanGame1.dbo.ChaInfo.ChaOnline as ChaOnline, 
                RanGame1.dbo.ChaInfo.ChaReborn as ChaReborn, 
                RanGame1.dbo.ChaInfo.ChaPkLoss as ChaPkLoss, 
                RanGame1.dbo.ChaInfo.ChaPkWin as ChaPkWin, 
                RanGame1.dbo.ChaInfo.LevelUpDate as LevelUpDate, 
                RanGame1.dbo.ChaInfo.ChaKills as ChaKills")
            ->leftJoin('RanUser.dbo.UserInfo', 'RanUser.dbo.UserInfo.UserNum', '=', 'RanGame1.dbo.ChaInfo.UserNum')
            ->leftJoin('RanGame1.dbo.GuildInfo', 'RanGame1.dbo.GuildInfo.GuNum', '=', 'RanGame1.dbo.ChaInfo.GuNum')
            ->where('RanUser.dbo.UserInfo.UserType', '1');

            if($class != null)
                $top->whereIn('ChaClass', Helper::_getClassDiff($class));

            if(isset($request->school))
                $top->where('ChaSchool', Helper::_getSchoolDiff($request->school));

            if(isset($request->sortBy)) {
                switch($request->sortBy) {
                    case 'level':
                        $top->orderBy('RanGame1.dbo.ChaInfo.ChaLevel', 'DESC');
                        $top->orderBy('RanGame1.dbo.ChaInfo.LevelUpDate', 'ASC');
                    break;
                    case 'wealth':
                        $top->orderBy('RanGame1.dbo.ChaInfo.ChaMoney', 'DESC');
                    break;
                    case 'pk':
                        $top->orderBy(DB::raw('RanGame1.dbo.ChaInfo.ChaPkWin'), 'DESC');
                        $top->orderBy(DB::raw('RanGame1.dbo.ChaInfo.ChaPkLoss'), 'ASC');
                    break;
                    case 'kills':
                        $top->orderBy('RanGame1.dbo.ChaInfo.ChaKills', 'DESC');
                    break;
                    case 'reborn':
                        $top->orderBy('RanGame1.dbo.ChaInfo.ChaReborn', 'DESC');
                    break;
                }
            } else {
                $top->orderBy('RanGame1.dbo.ChaInfo.ChaLevel', 'DESC');
                $top->orderBy('RanGame1.dbo.ChaInfo.LevelUpDate', 'ASC');
                $top->orderBy(DB::raw('RanGame1.dbo.ChaInfo.ChaPkWin'), 'DESC');
                $top->orderBy(DB::raw('RanGame1.dbo.ChaInfo.ChaPkLoss'), 'ASC');
            }
            
            $top->limit(50);
            
            $top = [
                'date' => time(),
                'data' => $top->simplePaginate(Helper::$_maxValues['rankings'])
            ];

            return $top;
        });

        $data['top'] = $topCharacters;
        $data['maxLevel'] = Helper::$_maxValues['level'];
        return view('rankings.index')->with($data);
    }

    protected function path($key) {
        $parts = array_slice(str_split($hash = md5($key), 2), 0, 2);

        //return $this->directory.'/'.join('/', $parts).'/'.$hash;
        return config('cache.stores.file.path') .'/'.join('/', $parts).'/'.$hash;
    }



    public function guild_badge($gu) {
        $g = DB::connection('RanGame1')
        ->table('dbo.GuildInfo')
        ->where('GuNum', explode('.', $gu)[0])
        ->first();
        return response(Helper::_generateBadge($g->GuMarkImage))
        ->header('Content-Type', 'image/png');
    }
}
