<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Helper;
use App\Rules\reCaptcha;
use DB;

use App\Model\Settings;

class UserController extends Controller
{
    public function register(Request $request) {
        if(Auth::check()) return redirect(route('home'));
        return view('home.register');
    }

    public function register_process(Request $request) {
        $request->validate([
            'username' => 'required|unique:RanUser.dbo.UserInfo,UserName|unique:RanUser.dbo.UserInfo,UserID',
            'email' => 'required|unique:RanUser.dbo.UserInfo,UserEmail|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'pincode' => 'required',
            'g-recaptcha-response' => [
                'required',
                new reCaptcha
            ]
        ]);
        
        DB::connection('RanUser')
        ->table('dbo.UserInfo')
        ->insert([
            'UserName' => $request->username,
            'UserID' => $request->username,
            'UserPass' => Helper::_password($request->password),
            'UserEmail' => $request->email,
            'UserPass2' => Helper::_password($request->pincode),
            'UserType' => 1,
            'UserIP' => $request->ip
        ]);
        
        return redirect(route('register'))->with([
            'success' => true,
            'msg' => "You've been successfull registered."
        ]);
    }

    public function change_password(Request $requet) {
        return view('account.change_password');
    }

    public function change_password_process(Request $request) {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_new_password' => 'required|same:new_password'
        ]);

        if(Helper::_password($request->old_password) != Auth::user()->UserPass) {
            return back()->with([
                'success' => false,
                'msg' => 'Current Password is incorrect.'
            ]);
        }

        DB::connection('RanUser')
        ->table('dbo.UserInfo')
        ->where('UserNum', Auth::user()->UserNum)
        ->update([
            'UserPass' => Helper::_password($request->new_password)
        ]);

        $this->_log([
            'action' => 'change_password',
            'message' => 'Successful Password Change'
        ]);
        
        return redirect(route('change_password'))->with([
            'success' => true,
            'msg' => 'Your password has been successfully changed.'
        ]);
    }

    public function change_email(Request $requet) {
        return view('account.change_email');
    }

    public function change_email_process(Request $request) {
        $request->validate([
            'old_email' => 'required|email',
            'new_email' => 'required|email',
            'confirm_new_email' => 'required|same:new_password|email'
        ]);

        if($request->old_email != Auth::user()->UserEmail) {
            return back()->with([
                'success' => false,
                'msg' => 'Current Email is incorrect.'
            ]);
        }

        DB::connection('RanUser')
        ->table('dbo.UserInfo')
        ->where('UserNum', Auth::user()->UserNum)
        ->update([
            'UserEmail' => $request->new_email
        ]);

        $this->_log([
            'action' => 'change_email',
            'message' => 'Successful Email Change'
        ]);
        
        return redirect(route('change_password'))->with([
            'success' => true,
            'msg' => 'Your password has been successfully changed.'
        ]);
    }
}
