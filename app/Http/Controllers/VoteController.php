<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Helper;
use App\Model\Settings;
use DB;

class VoteController extends Controller
{
    public function index() {
        $data['vote_links'] = DB::connection('RanComplexusWeb')
                        ->table('dbo.vote_links')
                        ->get();

        $data['last'] = DB::connection('RanComplexusWeb')
                ->table('dbo.vote_hitories')
                ->select([
                    'dbo.vote_hitories.ip_address as ip',
                    'dbo.vote_hitories.created_at as created_at',
                    'dbo.vote_links.name as name'
                ])
                ->join('dbo.vote_links', 'dbo.vote_links.id', '=', 'dbo.vote_hitories.vote_link_id')

                ->where('dbo.vote_hitories.userNum', Auth::user()->UserNum)
                ->orderBy('dbo.vote_hitories.created_at', 'DESC')
                ->get();

        $data['get_last'] = function($id) {
            $vote = DB::connection('RanComplexusWeb')
                ->table('dbo.vote_links')
                ->where('id', $id)
                ->first();

            $last = DB::connection('RanComplexusWeb')
                ->table('dbo.vote_hitories')
                ->where('vote_link_id', $id)
                ->where('userNum', Auth::user()->UserNum)
                ->orderBy('created_at', 'DESC')
                ->first();

            if($last) {
                $date = strtotime('+' . $vote->duration . ' hours', strtotime($last->created_at));
                if($date > time()) {
                  return false;
                }

                return true;
            }

            return true;
        };

        return view('home.vote', $data);
    }

    public function store(Request $request) {
        $request->validate([
            'id' => 'required|exists:RanComplexusWeb.dbo.vote_links,id'
        ]);


        $check_char = DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->where('UserNum', Auth::user()->UserNum)
            ->where('ChaLevel', '>=', \App\Settings::get('voting')['level_req'])
            ->first();

        if(!$check_char) {
            return redirect()->back()->with([
                'success' => false,
                'msg' => "You need to have 1 character at level " . \App\Settings::get('voting')['level_req']
            ]);
        }

        $vote = DB::connection('RanComplexusWeb')
            ->table('dbo.vote_links')
            ->where('id', $request->id)
            ->first();

        $last = DB::connection('RanComplexusWeb')
            ->table('dbo.vote_hitories')
            ->where('vote_link_id', $request->id)
            ->where('userNum', Auth::user()->UserNum)
            ->orderBy('created_at', 'DESC')
            ->first();

        if($last) {
            $date = strtotime('+' . $vote->duration . ' hours', strtotime($last->created_at));
            if($date > time()) {
                return redirect()->back()->with([
                    'success' => false,
                    'msg' => "Please try again later. You've already voted on this link."
                ]);
            }
       }
       
        DB::connection('RanComplexusWeb')
        ->table('dbo.Points')
        ->where('UserNum', Auth::user()->UserNum)
        ->update([
            'vPoints' =>  DB::raw('vPoints + ' . $vote->reward)
        ]);

        Helper::_logAction([
            'msg' => 'Successfully voted using #' . $vote->id,
            'action' => 'vote.success',
            'type' => 0,
        ]);

        DB::connection('RanComplexusWeb')
        ->table('dbo.vote_hitories')
        ->insert([
            'userNum' => Auth::user()->UserNum,
            'ip_address' => Helper::ip(),
            'vote_link_id' => $request->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        
        return \Redirect::to($vote->link);
    }
}
