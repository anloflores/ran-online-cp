<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Cache;
use DB;

class ShopController extends Controller
{
    public function index(Request $request) {

        $data['items'] = DB::connection('RanShop')
            ->table('dbo.ShopItemMap')
            ->select([
                'ProductNum',
                'ItemMain',
                'ItemSub',
                'ItemSS',
                'ItemName',
                'ItemPrice',
                'ItemSec',
                'ItemComment',
                'ItemDisc',
                'ItemCtg',
                'ItemStock',
            ])
            ->where('hidden', 0);

        if($request->name !== null) 
            $data['items']->where('ItemName', 'LIKE', '%'. $request->name .'%');

        if($request->category !== null) 
            $data['items']->where('ItemCtg', $request->category);

        if($request->type !== null) 
            $data['items']->where('ItemSec', $request->type);

        $data['items'] = $data['items']->get();

        return view('home.shop', $data);
    }

    public function purchase(Request $request) {
        $request->validate([
            'ProductNum' => 'required|exists:RanShop.ShopItemMap,ProductNum'
        ]);

        $baseItem = DB::connection('RanShop')
        ->table('dbo.ShopItemMap')
        ->where('ProductNum', $request->ProductNum);

        $item = $baseItem->first();

        if($item->hidden == 1) {
            Helper::_logAction([
                'msg' => 'Trying to buy a hidden item. ProductNum #' . $request->ProductNum,
                'action' => 'shop.purchase'    
            ]);

            return redirect()->back()->with([
                'success' => false,
                'msg' => 'Invalid Items.'
            ]);
        }

        $deductType = $item->ItemSec == 1 ? 'ep' : 'vp';
        $deductParams = [
            'type' => $deductType,
            'amount' => $item->ItemPrice,
            'UserNum' => Auth::user()->UserNum
        ];

        if($item->IsUnli == 0 && $item->Itemstock < 1) {
            Helper::_logAction([
                'msg' => 'Insufficient stocks. ProductNum #' . $request->ProductNum,
                'action' => 'shop.purchase'    
            ]);


            return redirect()->back()->with([
                'success' => false,
                'msg' => 'Insufficient stocks.'
            ]);
        }

        if(Helper::_deduct($deductParams)) {
            $updateData = [];

            if($item->IsUnli == 1)
                $updateData['Itemstock'] = $item->Itemstock-1;

            $updateData['ItemBought'] = $item->ItemBought+1;
            $baseItem->update($updateData);

            DB::connection('RanShop')
            ->table('dbo.ShopPurchase')
            ->insert([
                'UserUID' => Auth::user()->UserID,
                'ProductNum' => $item->ProductNum
            ]);

            Helper::_logAction([
                'msg' => 'Successfully bought ProductNum #' . $request->ProductNum,
                'action' => 'shop.purchase'    
            ]);

            return redirect()->back()->with([
                'success' => true,
                'msg' => 'You can now use the item you purhcase. Go to your account bank ingame.'
            ]);

        } else {

            Helper::_logAction([
                'msg' => 'Insufficient points. ProductNum #' . $request->ProductNum,
                'action' => 'shop.purchase'    
            ]);


            return redirect()->back()->with([
                'success' => false,
                'msg' => 'Insufficient points.'
            ]);
        }
    }
}
