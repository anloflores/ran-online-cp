<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class Helper extends Controller
{

    public static $_columns = array(
        'GameTime' => 'GameTime3'
    );

    public static  $_activeClasses = array(
		'brawler' => true,
		'swordsman' => true,
		'archer' => true,
		'shaman' => true,
		'extreme' => false,
		'gunner' => false,
		'assassin' => false,
		'magician' => false
    );

    public static $_activeUserFunctions = array(
        'change_school' => false,
        'gt2vp' => true,
    );
    
    public static  $_maxValues = array(
        'news' => 5,
        'rankings' => 10,
        'rankingsPage' => 10,
        'items' => 4,
        'level' => 210
    );

    public static  $_shopCategories = array(
        1 => 'Weapons',
		2 => 'Accessories',
		3 => 'Costumes',
		4 => 'Pet System',
		5 => 'EXP',
		6 => 'Cards',
		7 => 'Enhancements'
    );

    public static  $_activeShop = array(
        'vs' => true,
        'ps' => true
    );

    // Conversion
    public static $change_school = [
        'currency'  => 'gold',          // ep, vp, gold
        'amount'    =>  '1000000'       // value
    ];

    public static $ep_to_vp = [
        'min' => 1,                     // Min of 1 VP
        'conversion' => [1, 5]          // 1 EP to 5 VP
    ];

    public static $vp_to_ep = [
        'min' => 5,                     // Min of 1 VP
        'conversion' => [5, 1]          // 1 EP to 5 VP
    ];

    public static $gt_to_vp = [
        'min' => 60,                    // Min of 60 Minutes
        'conversion' => [60, 2]         // 60 Minutes GT to 1 VP
    ];

    // In Minutes
    public static  $_cacheTime = array(
        'topPlayer' => 60*12
    );

    public static  function _getClass($class) {
        switch($class){
			case 1:
			case 64:
				return "Brawler";
            break;
			case 2:
			case 128:
				return "Swordsman";
            break;
			case 4:
			case 256:
				return "Archer";
            break;
			case 8:
			case 512: 
				return "Shaman";
            break;
			case 1024:
			case 2048:
				return "Gunner";
            break;
			case 4096:
			case 8192:
				return "Assassin";
            break;
			case 16384:
			case 32768:
				return "Magician";
            break;
			default:
				return '';
		}
    }

    public static  function _getClassDiff($class) {
        switch($class){
			case 'brawler':
			    return [1,64];
            break;
			case 'swordsman':
			    return [2,28];
            break;
			case 'archer':
			    return [4,56];
            break;
			case 'shaman':
			    return [8,512];
            break;
			case 'gunner':
			    return [1024,48];
            break;
			case 'assassin':
			    return [4096,92];
            break;
			case 'magician':
			    return [16384,68];
            break;
			default:
				return '';
		}
    }

    public static function _password($str) {
        $type = 'md5';

        if($type == 'md5')
            return strtoupper(substr(md5($str), 0, 19));
        if($type == 'plain')
            return $str;

        return $str;
    }

    public static  function _getSchool($school) {
        switch ($school){
            case 0:
                return "SG";
            break;
            case 1:
                return "MP";
            break;
            case 2:
                return "PHX";
            break;
            default:
                return "Leonair";
		}
    }

    public static  function _getSchoolDiff($school) {
        switch ($school){
            case 'SG':
                return 0;
            break;
            case 'MP':
                return 1;
            break;
            case 'PHX':
                return 2;
            break;
            default:
                return 3;
		}
    }

    public static function _deduct($data) {
        switch($data['type']) {
            case 'gold':
                $g = DB::connection('RanGame1')
                ->table('dbo.ChaInfo')
                ->where('ChaNum', $data['ChaNum']);

                if($g->first()->ChaMoney < $data['amount']) {
                    return false;
                }

                $g->update([
                    'ChaMoney' => DB::raw('ChaMoney - ' . $data['amount'])
                ]);

                return true;
            break;

            case 'ep':
                $g = DB::table('dbo.Points')
                ->where('UserNum', $data['UserNum']);

                if($g->first()->ePoints < $data['amount']) {
                    return false;
                }

                $g->update([
                    'ePoints' => DB::raw('ePoints - ' . $data['amount'])
                ]);

                return true;
            break;

            case 'vp':
                $g = DB::table('dbo.Points')
                ->where('UserNum', $data['UserNum']);

                if($g->first()->vPoints < $data['amount']) {
                    return false;
                }

                $g->update([
                    'vPoints' => DB::raw('vPoints - ' . $data['amount'])
                ]);

                return true;
            break;

            case 'gt':
                $g = DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserNum', $data['UserNum']);

                $gt = \App\Settings::get('columns')['GameTime'];
                if($g->first()->$gt < $data['amount']) {
                    return false;
                }

                $g->update([
                    $gt  => DB::raw($gt . ' - ' . $data['amount'])
                ]);

                return true;
            break;
        }
    }

    public static function _add($data) {
        switch($data['type']) {
            case 'gold':
                $g = DB::connection('RanGame1')
                ->table('dbo.ChaInfo')
                ->where('ChaNum', $data['ChaNum'])
                ->update([
                    'ChaMoney' => DB::raw('ChaMoney + ' . $data['amount'])
                ]);

                return true;
            break;

            case 'ep':
                $g = DB::connection('RanComplexusWeb')
                ->table('dbo.Points')
                ->where('UserNum', $data['UserNum'])
                ->update([
                    'ePoints' => DB::raw('ePoints + ' . $data['amount'])
                ]);

                return true;
            break;

            case 'vp':
                $g = DB::connection('RanComplexusWeb')
                ->table('dbo.Points')
                ->where('UserNum', $data['UserNum'])
                ->update([
                    'vPoints' => DB::raw('vPoints + ' . $data['amount'])
                ]);

                return true;
            break;

            case 'gt':
                $g = DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserNum', $data['UserNum'])
                ->update([
                    'GameTime3' => DB::raw('GameTime3 + ' . $data['amount'])
                ]);

                return true;
            break;
        }
    }

   public static function _logAction($params) {
       $userNum = isset($params['userNum']) ? $params['userNum'] : (Auth::user()->UserNum ?? 0);
        if(isset($params['auth']) && $params['auth'] == 1) {
            $userNum = Auth::guard('admin')->user()->id;
        }
        
        DB::connection('RanComplexusWeb')
        ->table('dbo.Logs')
        ->insert([
            'userNum' => $userNum,
            'message' => $params['msg'] . ' ('. \Request::ip() .')',
            'action' => $params['action'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'type' => isset($params['type']) ? $params['type'] : (Auth::guard('admin')->check() ?? 0)
        ]);

        return true;
    }

    static function _generateBadge($hex) {
        $x = 16;
        $y = 11;

        $gd = imagecreatetruecolor($x, $y);
        $binary = bin2hex($hex);
        if($binary != '') {
            $currentY = 0;
            for( $y = 0 ; $y < 11 ; $y ++ ) {
                for( $x = 0 ; $x < 16 ; $x ++ ) {
                    $offset = $currentY*8*16 + $x*8;
                    // echo substr( $binary , $offset + 4 , 2 ) . substr( $binary , $offset + 2 , 2 ) . substr( $binary , $offset , 2 ) . '<br/>';
                    $color = imagecolorallocate($gd, hexdec(substr($binary, $offset+4, 2)), hexdec(substr($binary, $offset+2, 2)), hexdec(substr($binary, $offset, 2))); 
                    imagesetpixel($gd, $x, $y, $color);
                }
                $currentY++;
            }
        } 
        
        header("Content-Type: image/png");
        imagepng($gd);
    }
    
    public static function ip() {
        $ip = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
}
