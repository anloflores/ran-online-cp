<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Support\Facades\Auth;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function _log($data) {
        $data = array_merge($data, [
            'UserNum' => Auth::user()->UserNum,
            'type' => 0, 
            'created_at' => date('Y-m-d h:m:i'),
            'updated_at' => date('Y-m-d h:m:i'),
        ]);

        DB::table('Logs')
        ->insert($data);
    }
}
