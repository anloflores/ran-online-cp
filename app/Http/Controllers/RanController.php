<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Helper;
use App\Rules\reCaptcha;
use DB;
use App\Model\Settings;

class RanController extends Controller
{
    public function characters(Request $request) {
        $data['characters'] = Cache::remember('characters', 60, function() {
            return DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->where('UserNum', Auth::user()->UserNum)
            ->get();
        });
        return view('account.characters')->with($data);
    }

    public function change_school(Request $requet) {
        if(!Helper::$_activeUserFunctions['change_school'])
            return view('home.blank', [
                'title' => 'Change School',
                'content' => 'Change school is disabled.'
            ]);

         $data['characters'] = Cache::remember('characters', 60, function() {
            return DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->where('UserNum', Auth::user()->UserNum)
            ->get();
        });
        return view('account.change_school')->with($data);
    }

    public function change_school_process(Request $request) {

        if(!Helper::$_activeUserFunctions['change_school'])
            return view('home.blank', [
                'title' => 'Change School',
                'content' => 'Change school is disabled.'
            ]);

        $request->validate([
            'character' => 'required|exists:RanGame1.dbo.ChaInfo,ChaNum',
            'school' => 'required',
        ]);

        if($request->school > 2) return back()->with([
            'success' => false,
            'msg' => 'Invalid School'
        ]);

        $c = DB::connection('RanGame1')
        ->table('dbo.ChaInfo')
        ->selectRaw('ChaNum, ChaName, ChaSchool, ChaOnline')
        ->where('ChaNum', $request->character);

        if($c->first()->ChaOnline == 1) return back()->with([
            'success' => false,
            'msg' => 'Character is currently online. Character must be offline to change school'
        ]);

        $old = $c->first()->ChaSchool;

        if($c->first()->ChaSchool == $request->school) return back()->with([
            'success' => false,
            'msg' => "Current school is equal to New School."
        ]);

        $check = Helper::_deduct([
            'type' => Helper::$change_school['currency'],
            'ChaNum' => $request->character,
            'amount' => Helper::$change_school['amount']
        ]);

        if($check) {
            $c->update([
                'ChaSchool' => $request->school
            ]);

            $this->_log([
                'action' => 'change_school',
                'message' => 'Successful School Change of ' . $c->first()->ChaName . " from ". Helper::_getSchool($old) ." to ". Helper::_getSchool($request->school)
            ]);
        } else {
            return back()->with([
                'success' => false,
                'msg' => "Insufficient ". Helper::$change_school['currency']  . "."
            ]);
        }

        return back()->with([
            'success' => true,
            'msg' => "You've successfully changed school from ". Helper::_getSchool($old) ." to ". Helper::_getSchool($request->school) ."."
        ]);

    }


    // Conversions
    public function gt_to_vp(Request $request) {
        if(!Helper::$_activeUserFunctions['gt2vp'])
            return view('home.blank', [
                'title' => 'Gametime to Vote Points',
                'content' => 'Gametime to Vote Points is disabled.'
            ]);

        return view('account.conversion.gt_to_vp');
    } 

    public function gt_to_vp_process(Request $request) {

        $gt =  \App\Settings::get('columns')['GameTime'];
        $con = (int) \App\Settings::get('gt2vp')['conversion'][0];
        $multiplier = (int) \App\Settings::get('gt2vp')['conversion'][1];

        $current = (int) Auth::user()[$gt];

        $amount = floor($current / $con);
        $converted = (int) $amount * (int) $con;
        $balance = $current - $amount;

        if($amount < 1) 
            return redirect()->back()->with([
                'success' => false,
                'msg' => 'Game time is not enough.'
            ]);

        $addCheck = Helper::_add([
            'UserNum' => Auth::user()->UserNum,
            'amount' => $amount * $multiplier,
            'type' => 'vp'
        ]);

        $deductCheck = Helper::_deduct([
            'type' => 'gt',
            'amount' => $converted,
            'UserNum' => Auth::user()->UserNum
        ]);
        
        if(!$addCheck
        && !$deductCheck) {
            return redirect()->back()->with([
                'success' => false,
                'msg' => 'Something went wrong. Kindly Contact Admin.'
            ]);
        }

        Helper::_logAction([
            'msg' => 'Successfully converted ' . $converted . ' of game time to ' . $amount  . ' of vPoints.',
            'action' => 'gt2vp',
            'type' => 0    
        ]);
    
        return redirect()->back()->with([
            'success' => true,
            'msg' => "You've successfully convert " . $converted . " minutes of Game Time to " . $amount . " of vPoints."
        ]);
    }
}
