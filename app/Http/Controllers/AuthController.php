<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Auth;

use App\User;

use DB;

class AuthController extends Controller
{
    public function login(Request $request) {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('UserName', $request->username)
            ->where('UserPass', Helper::_password($request->password))
            ->first();

        if($user) {
            Auth::loginUsingId($user->UserNum);

            $g = DB::table('dbo.Points')
            ->where('UserNum', $user->UserNum)
            ->first();

            if($g == null) {
                DB::connection('RanComplexusWeb')
                ->table('dbo.Points')
                ->insert([
                    'UserNum' => $user->UserNum,
                    'vPoints' => 0,
                    'ePoints' => 0,
                    'created_at' => date('Y-m-d h:m:i'),
                    'updated_at' => date('Y-m-d h:m:i'),
                ]);
            }

            return redirect()->back();
        }

        return back()->withInput()->with([
            'error' => 'true',
            'msg' => 'Account does not exists.'
        ]);
    }

    public function logout() {
        \Illuminate\Support\Facades\Cache::flush();
        Auth::logout();
        return back();
    }
}
