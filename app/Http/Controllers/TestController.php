<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TestController extends Controller
{
    
    public function test() {
        $c = DB::connection('RanGame1')
        ->table('ChaInfo')
        ->where('ChaNum', 1)
        ->first();

        $items = strtoupper(bin2hex($c->ChaInven));
        echo $items;

    }

    function item_cut($string, $begin, $shortlength, $number=1) {   
		   $length = strlen($string);
		   if($length > ($shortlength * $number) )     
		   {
				   $end = $begin + $shortlength;
				   $flag = 0;
				   for($x=$begin; $x < $end; $x++){   
					   if(ord($string[$x]) <= 120) { 
					   $flag++;  
					   } 
				   }
					   if($flag%2==1){	 
					   $end++;    
					   }
		   $first_part = substr($string, 0, $end);
		   $last_part = substr($string, $end);
		   
		   $newstring = $first_part. "-" .$last_part;
		   $number++;		
		   return $this->item_cut($newstring, $end+1, $shortlength, $number);	
		   }else 
		   return $string;
	}
}