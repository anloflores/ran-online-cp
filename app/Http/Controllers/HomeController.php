<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use DB;

class HomeController extends Controller
{
    public function index() {
        $data['news'] = Cache::remember('news', 60, function() {
            return DB::connection('RanComplexusWeb')
            ->table('dbo.News')
            ->orderBy('created_at', 'DESC')
            ->where('type', '!=', '5')
            ->simplePaginate(Helper::$_maxValues['news']);
        });

        $data['items'] = DB::connection('RanShop')
            ->table('dbo.ShopItemMap')
            ->select([
                'ProductNum',
                'ItemMain',
                'ItemSub',
                'ItemSS',
                'ItemName',
                'ItemPrice',
                'ItemSec',
                'ItemComment',
                'ItemDisc',
                'ItemCtg',
                'ItemStock',
            ])
            ->where('hidden', 0)
            ->limit(4)
            ->get();
        
        return view('home.index')->with($data);
    }

    public function news($id) {
        $data['news'] = DB::connection('RanComplexusWeb')
        ->table('dbo.News')
        ->where('id', $id)
        ->first();

        return view('home.news', $data);
    }

    public function downloads() {
         $data['news'] = DB::connection('RanComplexusWeb')
        ->table('dbo.News')
        ->where('type', 5)
        ->orderBy('created_at', 'desc')
        ->first();

        return view('home.news', $data);
    }
}
