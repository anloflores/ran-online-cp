<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class AuthController extends Controller
{
    public function login() {
        if(Auth::guard('admin')->check()) {
            return redirect(route('cpanel.home'));
        }

        return view('cpanel.login');
    }

    public function login_process(Request $request) {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if(Auth::guard('admin')->attempt($request->only(['email', 'password']))) {
            return redirect(route('cpanel.home'));
        } else {
            return back()->with([
                'success' => false,
                'msg' => 'Invalid Account'
            ]);
        }
    }
}
