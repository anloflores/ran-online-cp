<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Helper;

class SendItemController extends Controller
{
    public function character() {
        return view('cpanel.send_item/character');
    }

    public function username() {
        return view('cpanel.send_item/username');
    }

    public function attendance() {
        return view('cpanel.send_item/attendance');
    }

    public function online() {
        return view('cpanel.send_item/online');
    }

    public function all() {
        return view('cpanel.send_item/all');
    }

    public function username_process(Request $request) {

        $request->validate([
            'ProductNum' => 'required|exists:RanShop.dbo.ShopItemMap,ProductNum',
            'UserName' => 'required|exists:RanUser.dbo.UserInfo,UserName',
        ]);

        $user = DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserName', $request->UserName)
                ->first();

        $item = DB::connection('RanShop')
                ->table('dbo.ShopItemMap')
                ->where('ProductNum', $request->ProductNum)
                ->first();

        $q = $request->quantity == null ? 1 : $request->quantity;
        foreach(range(1, $q) as $k) {
            DB::connection('RanShop')
            ->table('dbo.ShopPurchase')
            ->insert([
                'UserUID' => $user->UserID,
                'ProductNum' => $request->ProductNum,
            ]);
        }

        Helper::_logAction([
            'msg' => 'Successfully added an Item to UserName ' . $request->UserName . ' with an Item - '  . $item->ItemName,
            'action' => 'admin.put.items',
            'auth' => 1,  
        ]);

        return redirect()->back()->with([
            'success' => true,
            'msg' => "You've added an Item to User " . $request->UserName
        ]);
    }

    public function character_process(Request $request) {

        $request->validate([
            'ProductNum' => 'required|exists:RanShop.dbo.ShopItemMap,ProductNum',
            'ChaName' => 'required|exists:RanGame1.dbo.ChaInfo,ChaName'
        ]);

        $character = DB::connection('RanGame1')
                ->table('dbo.ChaInfo')
                ->where('ChaName', $request->ChaName)
                ->first();

        $user = DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserNum', $character->UserNum)
                ->first();

        $item = DB::connection('RanShop')
                ->table('dbo.ShopItemMap')
                ->where('ProductNum', $request->ProductNum)
                ->first();

        $q = $request->quantity == null ? 1 : $request->quantity;
        foreach(range(1, $q) as $k) {
            DB::connection('RanShop')
            ->table('dbo.ShopPurchase')
            ->insert([
                'UserUID' => $user->UserID,
                'ProductNum' => $request->ProductNum,
            ]);
        }

        Helper::_logAction([
            'msg' => 'Successfully added an Item to Character ' . $request->ChaName . ' with an Item - '  . $item->ItemName,
            'action' => 'admin.put.items'    
        ]);

        return redirect()->back()->with([
            'success' => true,
            'msg' => "You've added an Item to User " . $request->UserName
        ]);
    }

    public function attendance_process(Request $request) {

        $request->validate([
            'ProductNum' => 'required|exists:RanShop.dbo.ShopItemMap,ProductNum',
            'start' => 'required|date',
            'end' => 'required|date',
        ]);

        $attendance = DB::connection('RanGame1')
                ->table('dbo.Attendance')
                ->whereBetween('AttendDate', [
                    date('Y-m-d', strtotime($request->start)) . ' 01:00:00',
                    date('Y-m-d', strtotime($request->end)) . ' 23:59:59',
                ])->get();


        $item = DB::connection('RanShop')
            ->table('dbo.ShopItemMap')
            ->where('ProductNum', $request->ProductNum)
            ->first();

        foreach($attendance as $a) {
            $user = DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserNum', $a->UserNum)
                ->first();

            $q = $request->quantity == null ? 1 : $request->quantity;
            foreach(range(1, $q) as $k) {
                DB::connection('RanShop')
                ->table('dbo.ShopPurchase')
                ->insert([
                    'UserUID' => $user->UserID,
                    'ProductNum' => $request->ProductNum,
                ]);
            }

            Helper::_logAction([
                'msg' => 'Successfully added an Item to Username ' . $user->UserName . ' with an Item - '  . $item->ItemName . ' thru Attendance' ,
                'action' => 'admin.put.items'    
            ]);
        }


        Helper::_logAction([
            'msg' => 'Successfully added an Item to User who logged in on between ' . $request->start . ' and ' . $request->end ,
            'action' => 'admin.put.items'    
        ]);

        return redirect()->back()->with([
            'success' => true,
            'msg' => "You've added an Item to User " . $request->UserName
        ]);

    }


    public function online_process(Request $request) {

        $request->validate([
            'ProductNum' => 'required|exists:RanShop.dbo.ShopItemMap,ProductNum',
        ]);

        $characters = DB::connection('RanGame1')
                ->table('dbo.ChaInfo')
                ->where('ChaOnline', 1)
                ->get();

        

        $item = DB::connection('RanShop')
                ->table('dbo.ShopItemMap')
                ->where('ProductNum', $request->ProductNum)
                ->first();


        foreach($characters as $character) {
            $user = DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserNum', $character->UserNum)
                ->first();
                
            $q = $request->quantity == null ? 1 : $request->quantity;
            foreach(range(1, $q) as $k) {
                DB::connection('RanShop')
                ->table('dbo.ShopPurchase')
                ->insert([
                    'UserUID' => $user->UserID,
                    'ProductNum' => $request->ProductNum,
                ]);
            }

            Helper::_logAction([
                'msg' => 'Successfully added an Item to Character ' . $character->ChaName . ' with an Item - '  . $item->ItemName . ' thru Online',
                'action' => 'admin.put.items'    
            ]);
        }

        return redirect()->back()->with([
            'success' => true,
            'msg' => "You've added an Item to all online characters right now."
        ]);
    }

    public function all_process(Request $request) {

        $request->validate([
            'ProductNum' => 'required|exists:RanShop.dbo.ShopItemMap,ProductNum',
        ]);

        $users = DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->get();

        $item = DB::connection('RanShop')
                ->table('dbo.ShopItemMap')
                ->where('ProductNum', $request->ProductNum)
                ->first();


        foreach($users as $user) {
            DB::connection('RanShop')
                ->table('dbo.ShopPurchase')
                ->insert([
                    'UserUID' => $user->UserID,
                    'ProductNum' => $request->ProductNum
                ]);

            Helper::_logAction([
                'msg' => 'Successfully added an Item to Username ' . $user->UserName . ' with an Item - '  . $item->ItemName . ' thru Online',
                'action' => 'admin.put.items'    
            ]);
        }

        return redirect()->back()->with([
            'success' => true,
            'msg' => "You've added an Item to all users."
        ]);
    }
}
