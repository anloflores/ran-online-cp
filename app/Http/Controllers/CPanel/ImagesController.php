<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;

class ImagesController extends Controller
{
    public function index() {
        $data['imgs'] = DB::connection('RanComplexusWeb')
                    ->table('Images')
                    ->get();

        return view('cpanel.home/slider', $data);
    }

    public function store(Request $request) {
        $request->validate([
            'image' => 'required'
        ]);

        $fl = $request->image->getClientOriginalName();
        $fs = $request->image->getSize();
        $extension = pathinfo($fl, PATHINFO_EXTENSION);

        $filename = time() . '-'. md5($fl) .'.' . $extension;
        $request->image->move(public_path('/slider'), $filename);
        
        DB::connection('RanComplexusWeb')
        ->table('Images')
        ->insert([
            'name' => $filename
        ]);

        return redirect()->back()->with([
            'success' => true,
            'msg' => 'Successfully Uploaded an Image'
        ]);
    }

    public function delete(Request $request) {
        $request->validate([
            'id' => 'required|exists:RanComplexusWeb.dbo.Images,id'
        ]);

        DB::connection('RanComplexusWeb')
        ->table('Images')
        ->where('id', $request->id)
        ->delete();
        
        return redirect()->back()->with([
            'success' => true,
            'msg' => 'Successfully Deleted an Image'
        ]);
    }
}
