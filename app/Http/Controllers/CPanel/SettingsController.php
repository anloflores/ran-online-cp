<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SettingsController extends Controller
{
    public function index() {
        return view('cpanel.home/settings');
    }

    public function update(Request $request) {
        foreach($request->all() as $k => $r) {
            if($k == '_token') continue;
            
            $check = DB::connection('RanComplexusWeb')
                ->table('Settings')
                ->where('text', $k)
                ->first();

            if($check) {
                DB::connection('RanComplexusWeb')
                ->table('Settings')
                ->where('text', $k)
                ->update([
                    'value' => $r
                ]);
            } else {
                DB::connection('RanComplexusWeb')
                ->table('Settings')
                ->insert([
                    'text' => $k,
                    'value' => $r,
                ]);
            }
        }

        return redirect()->back()->with([
            'success' => true,
            'msg' => 'Successfully updated the settings'
        ]);
    }
}
