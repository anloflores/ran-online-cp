<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Helper;
use Illuminate\Http\Request;
use DB;

class ItemShopController extends Controller
{
    public function index(Request $request) {
        $data['items'] = DB::connection('RanShop')
            ->table('dbo.ShopItemMap')
            ->select([
                'ProductNum',
                'ItemMain',
                'ItemSub',
                'ItemSS',
                'ItemName',
                'ItemPrice',
                'ItemSec',
                'ItemComment',
                'ItemDisc',
                'ItemCtg',
                'ItemStock',
                'hidden',
            ])
            ->get();
        return view('cpanel.items.index', $data);
    }

    public function add(Request $request) {
        return view('cpanel.items.add');
    }

    public function store(Request $request) {
        $request->validate([
            'ItemName' => 'required',
            'ItemSS' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'ItemMain' => 'required|integer',
            'ItemSub' => 'required|integer',
            'ItemPrice' => 'required',
            'ItemSec' => 'required',
            'ItemCtg' => 'required',
            'IsUnli' => 'required',
            'Itemstock' => 'required_if:IsUnli,0',
            'hidden' => 'required',
        ]);

       if($request->ItemSS !== null) {
            $fl = $request->ItemSS->getClientOriginalName();
            $fs = $request->ItemSS->getSize();
            $extension = pathinfo($fl, PATHINFO_EXTENSION);

            $filename = time() . '-'. md5($fl) .'.' . $extension;
            $request->ItemSS->move(public_path('/items'), $filename);
       }

        $data = [
            'ItemName' => $request->ItemName,
            'ItemSS' => $filename ?? null,
            'ItemMain' => $request->ItemMain,
            'ItemSub' => $request->ItemSub,
            'ItemPrice' => $request->ItemPrice,
            'ItemSec' => $request->ItemSec,
            'ItemCtg' => $request->ItemCtg,
            'IsUnli' => $request->IsUnli,
            'Itemstock' => $request->Itemstock,
            'hidden' => $request->hidden,
        ];

        $id = DB::connection('RanShop')
        ->table('dbo.ShopItemMap')
        ->insertGetId($data);

        Helper::_logAction([
            'msg' => 'Successfully Added an Item Shop ProductNum #' . $id,
            'action' => 'admin.items',
            'auth' => 1,
        ]);

        return redirect(route('cpanel.items.index'))->with([
            'success' => true,
            'msg'   => 'Successfully added new Item'
        ]);
    }

    public function edit($id, Request $request) {
        $data['item'] = DB::connection('RanShop')
            ->table('dbo.ShopItemMap')
            ->select([
                'ProductNum',
                'ItemMain',
                'ItemSub',
                'ItemSS',
                'ItemName',
                'ItemPrice',
                'ItemSec',
                'ItemComment',
                'ItemDisc',
                'ItemCtg',
                'Itemstock',
                'IsUnli',
                'hidden',
            ])
            ->where('ProductNum', $id)
            ->first();

            // dd($data);

        return view('cpanel.items.edit', $data);
    }


    public function update($id, Request $request) {
        $request->validate([
            'ItemName' => 'required',
            'ItemSS' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'ItemMain' => 'required|integer',
            'ItemSub' => 'required|integer',
            'ItemPrice' => 'required',
            'ItemSec' => 'required',
            'ItemCtg' => 'required',
            'IsUnli' => 'required',
            'Itemstock' => 'required_if:IsUnli,0',
            'hidden' => 'required',
        ]);

        $base = DB::connection('RanShop')
        ->table('dbo.ShopItemMap')
        ->where('ProductNum', $id);

        $item = $base->first();
        $filename = $item->ItemSS;

        if($request->ItemSS !== null) {
            $fl = $request->ItemSS->getClientOriginalName();
            $fs = $request->ItemSS->getSize();
            $extension = pathinfo($fl, PATHINFO_EXTENSION);

            $filename = time() . '-'. md5($fl) .'.' . $extension;
            $request->ItemSS->move(public_path('/items'), $filename);
        }

        $data = [
            'ItemName' => $request->ItemName,
            'ItemSS' => $filename,
            'ItemMain' => $request->ItemMain,
            'ItemSub' => $request->ItemSub,
            'ItemPrice' => $request->ItemPrice,
            'ItemSec' => $request->ItemSec,
            'ItemCtg' => $request->ItemCtg,
            'IsUnli' => $request->IsUnli,
            'Itemstock' => $request->Itemstock,
            'hidden' => $request->hidden,
        ];

        $base->update($data);

        Helper::_logAction([
            'msg' => 'Successfully Updated Item Shop ProductNum #' . $id,
            'action' => 'admin.items',
            'auth' => 1,
        ]);

        return redirect(route('cpanel.items.index'))->with([
            'success' => true,
            'msg'   => 'Successfully update an Item'
        ]);
    }

    public function delete(Request $request) {
        $request->validate([
            'ProductNum' => 'required|exists:RanShop.dbo.ShopItemMap,ProductNum'
        ]);

        DB::connection('RanShop')
        ->table('dbo.ShopPurchase')
        ->where('ProductNum', $request->ProductNum)
        ->delete();

        DB::connection('RanShop')
        ->table('dbo.ShopItemMap')
        ->where('ProductNum', $request->ProductNum)
        ->delete();

        Helper::_logAction([
            'msg' => 'Successfully deleted an Item on ItemShop.',
            'action' => 'admin.items',
            'auth' => 1,
        ]);

        return redirect(route('cpanel.items.index'))->with([
            'success' => true,
            'msg' => 'Successfully deleted item #' . $request->ProductNum
        ]);
    }
}
