<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Helper;
use Illuminate\Http\Request;
use DB;


class MigrationController extends Controller
{
    public function seifer_migrate() {
        $data['check'] = null;

            try {
                $data['check'] = DB::connection('SeiferCP')
                ->table('Points')
                ->where('Points', '!=', 0)
                ->orWhere('VPoints', '!=', 0)
                ->get();
            } catch(\Illuminate\Database\QueryException $e){ 
                // return null;
            }
    
        return view('cpanel.ran.migrate', $data);
    }

    public function seifer_migrate_all_process(Request $request) {
        $points = null;
        try {
            $points = DB::connection('SeiferCP')
            ->table('dbo.Points')
            ->orWhere('Points', '!=', 0)
            ->orWhere('VPoints', '!=', 0)
            ->limit(200)
            ->get();
        } catch(Exception $e) {
            // return null;
        }
        
        $counter = 0;

        if($points) {
            foreach($points as $p) {
                // dd($c);
                // Points for ePoints
                // VPoints for vPotins
                
                $user = DB::connection('RanUser')
                    ->table('UserInfo')
                    ->where('UserName', $p->UserName)
                    ->first();

                if($user == null) continue;

                $chk = DB::connection('RanComplexusWeb')
                    ->table('Points')
                    ->where('UserNum', $user->UserNum);
                
                $ch = $chk->first();

                if(!$ch) {
                    // Insert if not exists
                    DB::connection('RanComplexusWeb')
                    ->table('Points')
                    ->insert([
                        'UserNum' => $user->UserNum,
                        'ePoints' => $p->Points,
                        'vPoints' => $p->VPoints,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                } else {
                    // Update if exists
                    $chk->update([
                        'ePoints' => DB::raw('ePoints + ' . $p->Points),
                        'vPoints' => DB::raw('vPoints + ' . $p->VPoints),
                    ]);
                }

                DB::connection('SeiferCP')
                ->table('dbo.Points')
                ->where('UserName', $p->UserName)
                ->update([
                    'Points' => 0,
                    'vPoints' => 0,
                ]);

                $counter++;
            }
        }

        Helper::_logAction([
            'msg' => 'Successfully Migrated ePoints and vPoints from SeiferCP ('. $counter .')',
            'action' => 'admin.migrate',
            'auth' => 1,
        ]);

        return redirect()->back()->with([
            'success' => true,
            'msg' => 'Successfully Migrated ePoints and vPoints from SeiferCP. Migrated ' . $counter . ' of records'
        ]);
    }
}
