<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;

class DownloadController extends Controller
{
    public function index() {
        $data['news'] = DB::connection('RanComplexusWeb')
        ->table('dbo.Downloads')
        ->orderBy('created_at', 'DESC')
        ->simplePaginate(10);

        return view('cpanel.downloads.index')->with($data);
    }

    public function create(Request $request) {
        return view('cpanel.downloads.create');
    }

    public function store(Request $request) {
        $request->validate([
            'link' => 'required',
            'provider' => 'required',
            'size' => 'required',
            'type' => 'required'
        ]);

        DB::connection('RanComplexusWeb')
        ->table('dbo.Downloads')
        ->insert([
            'link' => $request->link,
            'provider' => $request->provider,
            'size' => $request->size,
            'type' => $request->type,
            'created_at' => date('Y-m-d h:m:i'),
            'updated_at' => date('Y-m-d h:m:i')
        ]);
        
        return redirect(route('cpanel.downloads.index'));
    }

    public function edit($id, Request $request) {
        $data['news'] = DB::connection('RanComplexusWeb')
        ->table('dbo.Downloads')
        ->where('id', $id)
        ->first();

        return view('cpanel.downloads.edit')->with($data);
    }

    public function update($id, Request $request) {
        $request->validate([
            'link' => 'required',
            'provider' => 'required',
            'size' => 'required',
            'type' => 'required'
        ]);

        DB::connection('RanComplexusWeb')
        ->table('dbo.Downloads')
        ->where('id', $id)
        ->update([
            'link' => $request->link,
            'provider' => $request->provider,
            'size' => $request->size,
            'type' => $request->type,
            'updated_at' => date('Y-m-d h:m:i')
        ]);
        
        return redirect(route('cpanel.downloads.index'));
    }
}
