<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;

use DB;

class TopUpController extends Controller
{
    public function index(Request $request) {
        $data['tp'] = DB::connection('RanComplexusWeb')
                ->table('dbo.TopUps')
                ->get();

        $data['user'] = function($id) {
            if($id == 0) return 'For All';

            return DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserNum', $id)
                ->first()->UserName;
        };

        return view('cpanel.topup.index', $data);
    }

    public function add() {
        return view('cpanel.topup.add');
    }

    public function store(Request $request) {
        $request->validate([
            'UserName' => 'nullable|exists:RanUser.dbo.UserInfo,UserName',       
            'value' => 'required|integer' 
        ]);

        $userNum = 0;

        if($request->UserName != null) {
            $user = DB::connection('RanUser')
            ->table('dbo.UserInfo')
            ->where('UserName', $request->UserName)
            ->first();

            $userNum = $user->UserNum;
        }

        $data = [
            'UserNum' => $userNum,
            'code' => strtoupper(substr(md5(time()), 0, 15)),
            'pin' => rand(1000, 9999),
            'value' => $request->value,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'used' => 0,
        ];

        $id = DB::connection('RanComplexusWeb')
        ->table('dbo.TopUps')
        ->insertGetId($data);

        Helper::_logAction([
            'msg' => 'Successfully Generated a Top Up Code amounting '. $request->value .' with an ID of ' . $id,
            'action' => 'admin.topup',
            'auth' => 1,
        ]);

        return redirect(route('cpanel.topup.index'))->with([
            'success' => true,
            'msg' => 'Successfully generated a topup code and pin.'
        ]);
    }

    public function delete(Request $request) {
        $request->validate([
            'id' => 'required|exists:RanComplexusWeb.dbo.TopUps,id'
        ]);

        DB::connection('RanComplexusWeb')
        ->table('dbo.TopUps')
        ->where('id', $request->id)
        ->delete();

        Helper::_logAction([
            'msg' => 'Successfully Deleted a Top Up Code with an ID of ' . $request->id,
            'action' => 'admin.topup',
            'auth' => 1,
        ]);

        return redirect(route('cpanel.topup.index'))->with([
            'success' => true,
            'msg' => 'Successfully deleted a topup code and pin.'
        ]);
    }
}
