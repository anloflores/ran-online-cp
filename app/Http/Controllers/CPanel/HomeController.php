<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Artisan;
use DB;

class HomeController extends Controller
{
    public function index() {
        $data['accounts'] = DB::connection('RanUser')
                        ->table('dbo.UserInfo')
                        ->selectRaw('COUNT(*) as total')
                        ->first();

        $data['characters'] = DB::connection('RanGame1')
                        ->table('dbo.ChaInfo')
                        ->selectRaw('
                            SUM(CASE WHEN ChaClass IN(1,64) THEN 1 END) as brawler, 
                            SUM(CASE WHEN ChaClass IN(2,128) THEN 1 END) as swordsman, 
                            SUM(CASE WHEN ChaClass IN(4,256) THEN 1 END) as archer, 
                            SUM(CASE WHEN ChaClass IN(8,512) THEN 1 END) as shaman, 
                            SUM(CASE WHEN ChaClass IN(16,32) THEN 1 END) as extreme, 
                            SUM(CASE WHEN ChaClass IN(1024,2048) THEN 1 END) as gunner, 
                            SUM(CASE WHEN ChaClass IN(4096,8192) THEN 1 END) as assassin, 
                            SUM(CASE WHEN ChaClass IN(16384,32768) THEN 1 END) as magician,
                            SUM(CASE WHEN ChaDeleted = 1 THEN 1 END) as deleted,
                            SUM(CASE WHEN ChaDeleted = 0 THEN 1 END) as active,
                            SUM(CASE WHEN ChaOnline = 1 THEN 1 END) as online,
                            SUM(1) as total
                        ')
                        ->first();
        return view('cpanel.home.index')->with($data);
    }

    public function cache() {
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        return redirect()->back();
    }

    public function logs() {
        $data['logs'] = DB::connection('RanComplexusWeb')
                    ->table('dbo.Logs')
                    ->orderBy('created_at', 'DESC')
                    ->simplePaginate(50);

        return view('cpanel.home/logs', $data);
    }
}
