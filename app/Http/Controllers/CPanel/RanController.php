<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use DB;

class RanController extends Controller
{
    public function characters(Request $request) {
        $data = [];
        if($request->name !== null) {
            $data['characters'] = DB::connection('RanGame1')
            ->table('dbo.ChaInfo')
            ->where('ChaName', 'LIKE', '%'. $request->name .'%')
            ->simplePaginate(5);
        }

        return view('cpanel.ran.characters')->with($data);
    }

    public function users(Request $request) {
        $data = [];
        
        if($request->UserName !== null) {
            $data['user'] =  DB::connection('RanUser')
                ->table('dbo.UserInfo')
                ->where('UserName', $request->UserName)
                ->first();

            $data['points'] =  DB::connection('RanComplexusWeb')
                ->table('dbo.Points')
                ->where('UserNum', $data['user']->UserNum)
                ->first();

            $data['logs'] =  DB::connection('RanComplexusWeb')
                ->table('dbo.Logs')
                ->where('UserNum', $data['user']->UserNum)
                ->where('type', 0)
                ->get();

            $data['characters'] = DB::connection('RanGame1')
                        ->table('dbo.ChaInfo')
                        ->select([
                            '*',
                            'RanGame1.dbo.GuildInfo.GuName as ChaGuName',
                            'RanGame1.dbo.GuildInfo.GuNum as GuNum'
                        ])
                        ->where('UserNum', $data['user']->UserNum)
                        ->leftJoin('RanGame1.dbo.GuildInfo', 'RanGame1.dbo.GuildInfo.GuNum', '=', 'RanGame1.dbo.ChaInfo.GuNum')
                        ->get();
        }

        return view('cpanel.ran.users')->with($data);
    }

    public function give_epoints(Request $request) {
        return view('cpanel.ran.give.epoints');
    }

    public function give_vpoints(Request $request) {
        return view('cpanel.ran.give.vpoints');
    }

    public function give_gold(Request $request) {
        return view('cpanel.ran.give.epoints');
    }

    public function give_process(Request $request) {
        $request->validate([
            'username' => 'required',
            'amount' => 'required|integer',
            'type'  => 'required|in:1,2,3'
        ]);

        $user = DB::connection('RanUser')
        ->table('dbo.UserInfo')
        ->where('UserName', $request->username)
        ->first();

        if(!$user)
            return redirect()->back()->with([
                'success' => false,
                'msg'   => 'Username is invalid'
            ]);

        if(in_array($request->type, [1,2])) {
            $col = '';
            $red = 'epoints';
            switch($request->type) {
                case 1: $col = 'ePoints'; $red = 'ePoints'; break;
                case 2: $col = 'vPoints'; $red = 'vPoints'; break;
            }

            DB::connection('RanComplexusWeb')
            ->table('dbo.Points')
            ->where('UserNum', $user->UserNum)
            ->update([
                $col . '' =>  DB::raw($col . ' + ' . $request->amount)
            ]);

            Helper::_logAction([
                'msg' => 'Given ' . $red . ' worth ' . $request->amount,
                'action' => 'admin.give',
                'auth' => 1,
            ]);

            Helper::_logAction([
                'msg' => 'Received ' . $red . ' worth ' . $request->amount,
                'action' => 'admin.give',
                'auth' => 0,
                'userNum' => $user->UserNum,
                'type' => 0
            ]);

            return redirect(route('cpanel.give.' . strtolower($red)))->with([
                'success' => true,
                'msg'   => 'Successfully given a ' . $red
            ]);
        }
    }

}
