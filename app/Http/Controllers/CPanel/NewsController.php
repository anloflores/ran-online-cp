<?php

namespace App\Http\Controllers\CPanel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;

class NewsController extends Controller
{
    public function index() {
        $data['news'] = DB::connection('RanComplexusWeb')
        ->table('dbo.News')
        ->orderBy('created_at', 'DESC')
        ->simplePaginate(10);

        return view('cpanel.news.index')->with($data);
    }

    public function create(Request $request) {
        return view('cpanel.news.create');
    }

    public function store(Request $request) {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'content' => 'required'
        ]);

        DB::connection('RanComplexusWeb')
        ->table('dbo.News')
        ->insert([
            'UserNum' => Auth::guard('admin')->user()->id,
            'title' => $request->title,
            'type' => $request->type,
            'content' => $request->content,
            'created_at' => date('Y-m-d h:m:i'),
            'updated_at' => date('Y-m-d h:m:i')
        ]);
        
        return redirect(route('cpanel.news.index'));
    }

    public function edit($id, Request $request) {
        $data['news'] = DB::connection('RanComplexusWeb')
        ->table('dbo.News')
        ->where('id', $id)
        ->first();

        return view('cpanel.news.edit')->with($data);
    }

    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'content' => 'required'
        ]);

        DB::connection('RanComplexusWeb')
        ->table('dbo.News')
        ->where('id', $id)
        ->update([
            'UserNum' => Auth::guard('admin')->user()->id,
            'title' => $request->title,
            'type' => $request->type,
            'content' => $request->content,
            'updated_at' => date('Y-m-d h:m:i')
        ]);
        
        return redirect(route('cpanel.news.index'));
    }
}
