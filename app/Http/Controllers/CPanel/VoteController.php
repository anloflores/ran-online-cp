<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class VoteController extends Controller
{
    public function index() {
        $data['vote_links'] = DB::connection('RanComplexusWeb')
                        ->table('dbo.vote_links')
                        ->get();

        return view('cpanel.vote.index', $data);
    }

    public function add() {
        $data['vote_links'] = DB::connection('RanComplexusWeb')
                        ->table('dbo.vote_links')
                        ->get();

        return view('cpanel.vote.add', $data);
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'link' => 'required',
            'img' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'reward' => 'required|integer',
            'duration' => 'required|integer'
        ]);

        $fl = $request->img->getClientOriginalName();
        $fs = $request->img->getSize();
        $extension = pathinfo($fl, PATHINFO_EXTENSION);

        $filename = time() . '-'. md5($fl) .'.' . $extension;
        $request->img->move(public_path('/votes'), $filename);

        DB::connection('RanComplexusWeb')
        ->table('dbo.vote_links')
        ->insert([
            'name' => $request->name,
            'link' => $request->link,
            'img' => $filename,
            'reward' => $request->reward,
            'duration' => $request->duration
        ]);

        return redirect(route('cpanel.vote.index'))->with([
            'success' => true,
            'msg' => 'Successfully added new vote'
        ]);
    }

    public function edit($id, Request $request) {
        $data['v'] = DB::connection('RanComplexusWeb')
                        ->table('dbo.vote_links')
                        ->find($id);

        $data['id'] = $id;

        return view('cpanel.vote.edit', $data);
    }

    public function update(Request $request) {
        $request->validate([
            'id' => 'required|exists:RanComplexusWeb.dbo.vote_links,id',
            'name' => 'required',
            'link' => 'required',
            'img' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'reward' => 'required|integer',
            'duration' => 'required|integer'
        ]);
        
        $item = DB::connection('RanComplexusWeb')
        ->table('dbo.vote_links')
        ->where('id', $request->id)
        ->first();

        $filename = $item->img;

        if($request->ItemSS !== null) {
            $fl = $request->img->getClientOriginalName();
            $fs = $request->img->getSize();
            $extension = pathinfo($fl, PATHINFO_EXTENSION);

            $filename = time() . '-'. md5($fl) .'.' . $extension;
            $request->img->move(public_path('/votes'), $filename);
        }

        DB::connection('RanComplexusWeb')
        ->table('dbo.vote_links')
        ->where('id', $request->id)
        ->update([
            'name' => $request->name,
            'link' => $request->link,
            'img' => $filename,
            'reward' => $request->reward,
            'duration' => $request->duration
        ]);

        return redirect(route('cpanel.vote.index'))->with([
            'success' => true,
            'msg' => 'Successfully update vote #' . $request->id
        ]);
    }

    public function delete(Request $request) {
        $request->validate([
            'id' => 'required|exists:RanComplexusWeb.dbo.vote_links,id'
        ]);

        DB::connection('RanComplexusWeb')
        ->table('dbo.vote_links')
        ->where('id', $request->id)
        ->delete();

        return redirect(route('cpanel.vote.index'))->with([
            'success' => true,
            'msg' => 'Successfully deleted vote #' . $request->id
        ]);
    }
}
