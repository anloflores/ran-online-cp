<?php

namespace App\Http\Controllers\CPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index() {
        return view('cpanel.home.profile');
    }

    public function update(Request $request) {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'current_password' => 'required',
            'password' => 'nullable',
            'repassword' => 'nullable|same:password'
        ]);

        $check = DB::connection('RanComplexusWeb')
            ->table('Users')
            ->where('id', Auth::guard('admin')->user()->id)
            ->first();
        
        if(!$check || !Hash::check($request->current_password, $check->password))
            return redirect()->back()->with([
                'success' => false,
                'msg' => 'Invalid Action'
            ]);

        $data = [
            'name' => $request->name,
            'email' => $request->email,
        ];

        if($request->password != null) {
            $data['password'] = bcrypt($request->password);
        }

        DB::connection('RanComplexusWeb')
        ->table('Users')
        ->where('id', Auth::guard('admin')->user()->id)
        ->update($data);

        Helper::_logAction([
            'msg' => 'Successfully updated profile.',
            'action' => 'admin.profile',
            'auth' => 1,  
        ]);

        return redirect()->back()->with([
            'success' => true,
            'msg' => 'Successfully updated your profile.'
        ]);
    }
}
