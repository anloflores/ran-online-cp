<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Helper;
use App\Model\Settings;
use DB;

class TopUpController extends Controller
{
    public function index() {
        return view('home.topup');
    }

    public function store(Request $request) {
        $request->validate([
            'code'  => 'required',
            'pin'   => 'required'
        ]);

        $check = DB::connection('RanComplexusWeb')
        ->table('dbo.TopUps')
        ->where('code', $request->code)
        ->where('pin', $request->pin)
        ->where('used', 0);

        if($check->first()) {
            $item = $check->first();

            if($item->UserNum != 0 && $item->UserNum != Auth::user()->UserNum) {
                return redirect()->back()->with([
                    'success' => false,
                    'msg'   => 'Invalid Code and PIN'
                ]);
            }

            $check->update([
                'used' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            DB::connection('RanComplexusWeb')
            ->table('dbo.Points')
            ->where('UserNum', Auth::user()->UserNum)
            ->update([
                'ePoints' =>  DB::raw('ePoints + ' . $item->value)
            ]);

            Helper::_logAction([
                'msg' => 'Successfully used Topup with an ID of ' . $item->id,
                'action' => 'topup.success',
                'type' => 0,
            ]);
                

            return redirect()->back()->with([
                'success' => true,
                'msg'   => 'Successfully used Topup Code and Pin worth ' . $item->value
            ]);

        } else {
            return redirect()->back()->with([
                'success' => false,
                'msg'   => 'Invalid Code and PIN'
            ]);
        }
    }
}
