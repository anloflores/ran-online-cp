<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class Settings extends Model {
    public static function get($key) {
        $_get = function($k) {
            $d = DB::connection('RanComplexusWeb')
            ->table('Settings')
            ->where('text', $k)
            ->first();

            return $d->value ?? null;
        };

        $data = [
            'title' => $_get('title') ?? 'Ran Online',
            'description' => $_get('description') ?? 'Ran Online',
            'online_multiplier' => (float) $_get('online_multiplier') ?? 1.6,
            
            'columns' => [
                'GameTime' => $_get('col_gametime') ?? 'Gametime3'
            ],

            'voting' => [
                'level_req' =>  $_get('vote_level_req') ?? 210,
            ],
            
            'gt2vp'  => [
                'conversion' => [
                    $_get('gt2vp_worth') ?? 60,
                    $_get('gt2vp_equivalent') ?? 1
                ]      
            ],

        ];

        return $data[$key];
    }
}
