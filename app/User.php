<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    protected $connection= 'RanUser';
    protected $table = 'dbo.UserInfo';
    protected $primaryKey = 'UserNum';

    protected $hidden = ['UserPass', 'UserPass2'];
}
