<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{

    public static $gametime_col = 'GameTime2';

    public static $change_school = [
        'currency'  => 'gold',          // ep, vp, gold
        'amount'    =>  '1000000'       // value
    ];

    public static $ep_to_vp = [
        'min' => 1,                     // Min of 1 VP
        'conversion' => [1, 5]          // 1 EP to 5 VP
    ];

    public static $vp_to_ep = [
        'min' => 5,                     // Min of 1 VP
        'conversion' => [5, 1]          // 1 EP to 5 VP
    ];

    public static $gt_to_vp = [
        'min' => 60,                    // Min of 60 Minutes
        'conversion' => [60, 2]         // 60 Minutes GT to 1 VP
    ];
}
