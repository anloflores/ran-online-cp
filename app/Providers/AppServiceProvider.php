<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('_config', function($key) {
            return config('ran.columns.' . $key);
        });

        view()->share('_settings', function($key) {
            $s = new \App\Settings();
            return  $s->get($key);
        });
    }
}
