<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dbo.Users')
        ->insert([
            'name' => 'Carlo Flores',
            'email' => 'admin@complexus.net',
            'password' => bcrypt('password')
        ]);
    }
}
